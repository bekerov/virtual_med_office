<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 05.12.14
 * Time: 17:53
 */

class AppointmentStatuses extends Eloquent {
    protected $table = 'AppointmentStatuses';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}