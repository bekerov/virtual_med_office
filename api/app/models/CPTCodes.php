<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 17.11.14
 * Time: 17:45
 */

class CPTCodes extends Eloquent {
    protected $table = 'CPTCodes';
    protected $primaryKey = 'CPTCodeID';
    public $timestamps = false;
}