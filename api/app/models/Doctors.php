<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur
 * Date: 04.11.14
 * Time: 23:24
 */

class Doctors extends Eloquent {
    protected $table = 'Doctors';
    protected $primaryKey = 'DoctorID';
    public $timestamps = false;
}