<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 12.11.14
 * Time: 17:45
 */

class BillingTemplate extends Eloquent {
    protected $table = 'BIllingTemplate';
    protected $primaryKey = 'TemplateID';
    public $timestamps = false;
}