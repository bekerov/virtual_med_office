<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 06.11.14
 * Time: 17:45
 */

class Cases extends Eloquent {
    protected $table = 'Cases';
    protected $primaryKey = 'CaseID';
    public $timestamps = false;

    public function doctor() {
        return $this->belongsTo('Doctors', 'DoctorID');
    }
    public function attorney() {
        return $this->belongsTo('Attorneys', 'AttorneyID');
    }
    public function primary_insurance() {
        return $this->belongsTo('Insurances', 'PrimaryInsuranceID');
    }
    public function secondary_insurance() {
        return $this->belongsTo('Insurances', 'SecondaryInsuranceID');
    }
    public function bill_batches() {
        return $this->hasMany('BillBatches', 'BatchID');
    }
    public function bills() {
        return $this->hasManyThrough('Bills', 'BillBatches', 'CaseID', 'BatchID');
    }
    public function insurances() {
        return $this->hasMany('CaseInsurances', 'CaseID');
    }
//    public function insurances() {
//        return $this->hasManyThrough('CaseInsurances', 'Insurances', 'InsuranceID', 'CaseID');
//    }
}

