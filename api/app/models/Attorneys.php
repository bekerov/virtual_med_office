<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 06.11.14
 * Time: 17:45
 */

class Attorneys extends Eloquent {
    protected $table = 'Attorneys';
    protected $primaryKey = 'AttorneyID';
    public $timestamps = false;
}