<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 17.11.14
 * Time: 22:19
 */

class BillStatus extends Eloquent {
    protected $table = 'BillStatus';
    protected $primaryKey = 'BillStatusID';
    public $timestamps = false;
}