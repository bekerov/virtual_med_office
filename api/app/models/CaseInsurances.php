<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 21.11.14
 * Time: 13:42
 */

class CaseInsurances extends Eloquent {
    protected $table = 'caseinsurances';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function insurances() {
        return $this->belongsTo('Insurances', 'InsuranceID');
    }
}