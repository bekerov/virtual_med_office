<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 12.11.14
 * Time: 17:45
 */

class BillingTemplateDesc extends Eloquent {
    protected $table = 'BillingTemplateDesc';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}