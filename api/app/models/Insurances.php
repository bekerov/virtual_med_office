<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 21.11.14
 * Time: 13:42
 */

class Insurances extends Eloquent {
    protected $table = 'Insurances';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}