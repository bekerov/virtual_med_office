<?php
/**
 * User: Pershanin Dmitry
 * Date: 20.11.14
 */

class Ehr extends Eloquent {
    protected $table = 'EHR';
    protected $primaryKey = 'EhrID';
    public $timestamps = false;
}