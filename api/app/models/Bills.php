<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 06.11.14
 * Time: 17:45
 */

class Bills extends Eloquent {
    protected $table = 'Bills';
    protected $primaryKey = 'BallID';
    public $timestamps = false;
}