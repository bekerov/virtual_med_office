<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 06.11.14
 * Time: 17:45
 */

class Calendars extends Eloquent {
    protected $table = 'Calendars';
    protected $primaryKey = 'CalendarID';
    public $timestamps = false;
}