<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 06.11.14
 * Time: 17:45
 */

class BillBatches extends Eloquent {
    protected $table = 'BillBatches';
    protected $primaryKey = 'BatchID';
    public $timestamps = false;

    public function statuses() {
        return $this->belongsTo('BatchStatuses', 'StatusID');
    }

    public function calendars() {
        return $this->belongsTo('Calendars', 'CalendarID');
    }

}