<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 06.11.14
 * Time: 17:45
 */

class BatchStatuses extends Eloquent {
    protected $table = 'BatchStatuses';
    protected $primaryKey = 'StatusID';
    public $timestamps = false;

    public function BillBatches() {
        return $this->hasMany('BillBatches', 'StatusID');
    }
}