<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 06.11.14
 * Time: 17:45
 */

class CaseType extends Eloquent {
    protected $table = 'CaseType';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}