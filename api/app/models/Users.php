<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 11.11.14
 * Time: 12:32
 */

class Users extends Eloquent {
    protected $table = 'Users';
    protected $primaryKey = 'UserID';
    public $timestamps = false;
}