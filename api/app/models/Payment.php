<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 19.11.14
 * Time: 17:28
 */

class Payment extends Eloquent {
    protected $table = 'Payment';
    protected $primaryKey = 'PaymentID';
    public $timestamps = false;
}