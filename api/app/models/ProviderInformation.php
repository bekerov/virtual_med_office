<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 06.11.14
 * Time: 17:45
 */

class ProviderInformation extends Eloquent {
    protected $table = 'ProviderInformation';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}