<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 04.12.14
 * Time: 15:47
 */

class CalEvents extends Eloquent {
    protected $table = 'CalEvents';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}