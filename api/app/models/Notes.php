<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur
 * Date: 04.11.14
 * Time: 23:24
 */

class Notes extends Eloquent {
    protected $table = 'Notes';
    protected $primaryKey = 'NotesID';
    public $timestamps = false;
}