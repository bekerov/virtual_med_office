<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur
 * Date: 04.11.14
 * Time: 23:24
 */

class Patients extends Eloquent {
    protected $table = 'Patients';
    protected $primaryKey = 'PatientID';
    public $timestamps = false;
}