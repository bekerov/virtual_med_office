<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
    return 'Medcrm api';
});

// Patients routes
Route::get('patients', 'PatientsController@getPatients');
Route::get('patients/{id}', 'PatientsController@getPatient');
Route::post('patients', 'PatientsController@addPatient');
Route::put('patients/{id}', 'PatientsController@editPatient');
Route::delete('patients/{id}', 'PatientsController@deletePatient');

// Attorneys routes
Route::get('attorneys', 'AttorneyController@getAttorneys');
Route::get('attorneys/{id}', 'AttorneyController@getAttorney');
Route::post('attorneys', 'AttorneyController@addAttorney');
Route::put('attorneys/{id}', 'AttorneyController@editAttorney');
Route::delete('attorneys/{id}', 'AttorneyController@statusAttorney');

// Provider routes
Route::get('provider', 'ProviderController@getProviders');
Route::get('provider/{id}', 'ProviderController@getProvider');
Route::put('provider/{id}', 'ProviderController@editProvider');

// Cases routes
Route::get('cases', 'CasesController@getAllCases');
Route::get('cases/{id}', 'CasesController@getCases');
Route::post('cases/{id}', 'CasesController@addCase');

// Single case
Route::get('case/{id}', 'CasesController@getCase');
Route::put('case/{id}', 'CasesController@editCase');

// Billing
Route::get('billing', 'BillingController@getBillings');
Route::get('billing/{id}', 'BillingController@getBilling');
Route::get('add_billing/{id}', 'BillingController@getBillings');
Route::post('billing/{id}', 'BillingController@addBill');
Route::post('edit_billing/{id}', 'BillingController@editBill');


//Payments
Route::get('payments', 'CollectionController@getAllPayments');
Route::get('payments/{id}', 'CollectionController@getPaymentsById');

// Collection
Route::get('billbatches/{case_id}', 'CollectionController@getBillBatchesById');
Route::get('bills/{batch_id}', 'CollectionController@getBillsById');
//Route::get('calendars/{calendar_id}', 'CollectionController@getCalendarsById');

// Collection Payment
Route::get('payment', 'PaymentController@getPayment'); 

// Calendars
Route::get('calendars', 'CalendarsController@getCalendars');
Route::get('calendars/{id}', 'CalendarsController@getCalendar');
Route::post('calendars', 'CalendarsController@addCalendar');
Route::put('calendars/{id}', 'CalendarsController@editCalendar');
Route::delete('calendars/{id}', 'CalendarsController@statusCalendar');

// Doctors
Route::get('doctors', 'DoctorsController@getDoctors');
Route::get('doctors/{id}', 'DoctorsController@getDoctor');
Route::post('doctors', 'DoctorsController@addDoctor');
Route::put('doctors/{id}', 'DoctorsController@editDoctor');

//Case Insurances
//Route::get('caseinsurances', 'CasesController@getInsurances');
Route::post('caseinsurances/{id}', 'CasesController@addCaseInsurance');
Route::put('caseinsurances/{id}', 'CasesController@editCaseInsurance');

//Insurances
Route::get('insurances', 'CasesController@getInsurances');
Route::get('insurances/{id}', 'CasesController@getInsurance');
Route::post('insurances', 'CasesController@addInsurance');

// Case types
Route::get('casetypes', 'CaseTypesController@getAllCaseTypes');
Route::get('casetypes/{id}', 'CaseTypesController@getCaseType');
Route::post('casetypes', 'CaseTypesController@addCaseType');
Route::put('casetypes/{id}', 'CaseTypesController@editCaseType');

// Billing template
Route::get('billing_template', 'BillingTemplateController@getBillingTemplates');
Route::get('billing_template/{id}', 'BillingTemplateController@getBillingTemplate');
Route::post('billing_template', 'BillingTemplateController@addBillingTemplate');
Route::post('billing_template/{id}', 'BillingTemplateController@editBillingTemplate');
Route::delete('billing_template/{id}', 'BillingTemplateController@statusBillingTemplate');

// CPTcode
Route::get('cptcodes', 'CPTCodesController@getCPTCode');
Route::get('cptcodes/{id}', 'CPTCodesController@getCPTCode');

// Users
Route::get('users', 'UsersController@getUsers');
Route::get('users/{id}', 'UsersController@getUser');
Route::post('users', 'UsersController@addUser');
Route::put('users/{id}', 'UsersController@editUser');
Route::delete('users/{id}', 'UsersController@statusUser');


// EHR
Route::get('ehr', 'EhrController@getEhrs');
Route::get('newehrdn/{id}', 'NewEhrDnController@getEhr');
Route::post('newehrdn', 'NewEhrDnController@addEhr');
Route::get('new-medical-report/{id}', 'NewMedicalReportController@getEhr');

// Calendar events
Route::get('calevents', 'CalEventsController@getEvents');
Route::get('calevents/{id}', 'CalEventsController@getEvent');
Route::post('calevents', 'CalEventsController@addEvent');

// Appointment statuses
Route::get('appointment_statuses', 'CalEventsController@getAppointmentStatuses');

