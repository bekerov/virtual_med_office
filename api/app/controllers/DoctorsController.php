<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 12.11.14
 * Time: 11:47
 */

class DoctorsController extends Controller {
    protected function getDoctors() {
        $results = Doctors::all(array('DoctorID', 'Firstname', 'Lastname'));
        return $results;
    }

    protected function getDoctor($id) {
        $results = Doctors::find($id);
        return $results;
    }

    protected function addDoctor() {
        try {
            $calendar = new Doctors;
            $calendar->ParentID = Input::get('ParentID');
            $calendar->Practice = Input::get('Practice');
            $calendar->Firstname = Input::get('Firstname');
            $calendar->Lastname = Input::get('Lastname');
            $calendar->Specialty = Input::get('Specialty');
            $calendar->NPI = Input::get('NPI');
            $calendar->License = Input::get('License');
            $calendar->Address = Input::get('Address');
            $calendar->Address2 = Input::get('Address2');
            $calendar->City = Input::get('City');
            $calendar->State = Input::get('State');
            $calendar->Zip = Input::get('Zip');
            $calendar->Phone = Input::get('Phone');
            $calendar->Phone2 = Input::get('Phone2');
            $calendar->Fax = Input::get('Fax');
            $calendar->Email = Input::get('Email');
            $calendar->Username = Input::get('Username');
            $calendar->Password = Input::get('Password');
            $calendar->save();

            //todo Добавить емейл доктору из файла addEditDoctor.php

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

    protected function editDoctor($id) {
        try {
            $calendar = Doctors::find($id);
            $calendar->ParentID = Input::get('ParentID');
            $calendar->Practice = Input::get('Practice');
            $calendar->Firstname = Input::get('Firstname');
            $calendar->Lastname = Input::get('Lastname');
            $calendar->Specialty = Input::get('Specialty');
            $calendar->NPI = Input::get('NPI');
            $calendar->License = Input::get('License');
            $calendar->Address = Input::get('Address');
            $calendar->Address2 = Input::get('Address2');
            $calendar->City = Input::get('City');
            $calendar->State = Input::get('State');
            $calendar->Zip = Input::get('Zip');
            $calendar->Phone = Input::get('Phone');
            $calendar->Phone2 = Input::get('Phone2');
            $calendar->Fax = Input::get('Fax');
            $calendar->Email = Input::get('Email');
            $calendar->Username = Input::get('Username');
            $calendar->Password = Input::get('Password');
            $calendar->save();

            //todo Добавить емейл доктору из файла addEditDoctor.php

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }
//    protected function deletePatient($id) {
////        try {
////            $patient = Attorneys::find($id);
////            $patient->IsDeleted = Input::get('IsDeleted');
////            $patient->save();
////
////        } catch(PDOException $exception) {
////            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
////        }
////        return  '{"status" : "success", "data" : null } ';
//    }

}
