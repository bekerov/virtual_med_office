<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 12.11.14
 * Time: 11:47
 */

class CalendarsController extends Controller {
    protected function getCalendars() {
        $results = Calendars::all();
        return $results;
    }

    protected function getCalendar($id) {
        $results = Calendars::find($id);
        return $results;
    }

    protected function addCalendar() {
        try {
            $calendar = new Calendars;
            $calendar->firstName = Input::get('firstName');
            $calendar->middleName = Input::get('middleName');
            $calendar->lastName = Input::get('lastName');
            $calendar->Description = Input::get('Description');
            $calendar->Specialty = Input::get('Specialty');
            $calendar->NPI = Input::get('NPI');
            $calendar->License = Input::get('License');
            $calendar->BgColor = Input::get('BgColor');
            $calendar->CalColor = Input::get('CalColor');
            $calendar->bordercolor = Input::get('bordercolor');
            $calendar->blockbackground = Input::get('blockbackground');
            $calendar->blockbordercolor = Input::get('blockbordercolor');
            $calendar->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

    protected function editCalendar($id) {
        try {
            $calendar = Calendars::find($id);
            $calendar->firstName = Input::get('firstName');
            $calendar->middleName = Input::get('middleName');
            $calendar->lastName = Input::get('lastName');
            $calendar->Description = Input::get('Description');
            $calendar->Specialty = Input::get('Specialty');
            $calendar->NPI = Input::get('NPI');
            $calendar->License = Input::get('License');
            $calendar->BgColor = Input::get('BgColor');
            $calendar->CalColor = Input::get('CalColor');
            $calendar->bordercolor = Input::get('bordercolor');
            $calendar->blockbackground = Input::get('blockbackground');
            $calendar->blockbordercolor = Input::get('blockbordercolor');
            $calendar->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

    protected function statusCalendar($id) {
        try {
            $patient = Calendars::find($id);
            $patient->IsDeleted = Input::get('status');
            $patient->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

}
