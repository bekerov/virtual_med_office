<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 09.11.14
 * Time: 2:41
 */

class CollectionController extends Controller {

    protected function getBillBatchesById($id) {
        $results = BillBatches::where('CaseID', '=', $id)->with('calendars')->get();
        foreach ($results as $result) {
            $result->bills = Bills::where('BatchID', '=', $result->BatchID)->get();
            foreach ($result->bills as $bill) {
                $bill->code_amount = CPTCodes::where('CPTCode', '=', $bill->CPTCode)->get(array('Amount'))->first();
                $bill->status = BillStatus::where('BillStatusID', '=', $bill->Status)->get(array('Name'))->first();
            }
        }
        return $results;
    }

    protected function getBillsById($id) {
        $results = Bills::where('BatchID', '=', $id)->get();
        return $results;
    }

    protected function getCalendarsById($id) {
        $results = Calendars::where('CalendarID', '=', $id)->get();
        return $results;
    }

    protected function getAllPayments() {
        $results = Payment::all();
        return $results;
    }

    protected function getPaymentsById($id) {
        // get all payments with batch id
        $results = Payment::where('BatchIDs', '=', $id)->get();
        return $results;
    }



}
