<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 09.11.14
 * Time: 15:00
 */

class CaseTypesController extends Controller {
    protected function getAllCaseTypes() {
        $results = CaseType::all();
        return $results;
    }

    protected function getCaseType($id) {
        $results = CaseType::find($id);
        return $results;
    }

    protected function addCaseType(){
        try {
            // Add case type
            $type = new CaseType;
            $type->Name = Input::get('Name');
            $type->DisOrder = Input::get('DisOrder');
            $type->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

    protected function editCaseType($id) {
        try {
            $type = CaseType::find($id);
            $type->Name = Input::get('Name');
            $type->DisOrder = Input::get('DisOrder');
            $type->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

}
