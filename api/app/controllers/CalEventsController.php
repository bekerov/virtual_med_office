<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 04.12.14
 * Time: 15:50
 */

class CalEventsController extends Controller {
    protected function getEvents() {
//        $results = CalEvents::all();
        $results = DB::table('CalEvents')->select(array('ID as id', 'Title as title', 'StartTime as start', 'EndTime as end'))->get();
        return $results;
    }

//    protected function getCalendar($id) {
//        $results = Calendars::find($id);
//        return $results;
//    }
//
    protected function addEvent() {
        $StartTime = Input::get('StartTime');
        $duration = Input::get('duration');
        $duration = strtotime($duration);

        $hour = date('H', $duration);
        $minutes = date('i', $duration);
        $EndTime = date('Y-m-d H:i:s Z',strtotime('+'.$hour.' hour +'.$minutes.' minutes',strtotime($StartTime)));

        try {
            $item = new CalEvents;
            $item->Title = Input::get('Title');
            $item->StartTime = $StartTime;
            $item->EndTime = $EndTime;
            $item->CalendarID = Input::get('CalendarID');
            $item->duration = $minutes;
            $item->doctortype = Input::get('CalendarID');
            $item->blocktype = Input::get('blocktype');
            $item->is_block = '1';
            $item->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }
//
//    protected function editCalendar($id) {
//        try {
//            $item = Calendars::find($id);
//            $item->firstName = Input::get('firstName');
//            $item->middleName = Input::get('middleName');
//            $item->lastName = Input::get('lastName');
//            $item->Description = Input::get('Description');
//            $item->Specialty = Input::get('Specialty');
//            $item->NPI = Input::get('NPI');
//            $item->License = Input::get('License');
//            $item->BgColor = Input::get('BgColor');
//            $item->CalColor = Input::get('CalColor');
//            $item->bordercolor = Input::get('bordercolor');
//            $item->blockbackground = Input::get('blockbackground');
//            $item->blockbordercolor = Input::get('blockbordercolor');
//            $item->save();
//
//        } catch(PDOException $exception) {
//            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
//        }
//        return  '{"status" : "success", "data" : null } ';
//    }
//
//    protected function statusCalendar($id) {
//        try {
//            $patient = Calendars::find($id);
//            $patient->IsDeleted = Input::get('status');
//            $patient->save();
//
//        } catch(PDOException $exception) {
//            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
//        }
//        return  '{"status" : "success", "data" : null } ';
//    }

    protected function getAppointmentStatuses() {
        $results = AppointmentStatuses::all();
        return $results;
    }

}