<?php
/**
 * User: Pershanin Dmitry
 * Date: 20.11.14
 */

class NewMedicalReportController extends Controller {
    protected function getEhrs() {
        $results = DB::select(' select c.CaseID, c.PatientID, c.DOA, p.FirstName, p.LastName, p.DOB, p.SocialSecurityNumber 
								from Cases as c, Patients as p, Ehr as e 
								where c.PatientID=e.PatientID and c.CaseID=e.CaseID GROUP BY e.CaseID');
        foreach ($results as $result) {
            $result->DOA = ($result->DOA != NULL) ? $result->DOA : '-';
            $result->DOB = ($result->DOB != NULL) ? $result->DOB : '-';
            $result->ehri = DB::select('select e.EhrID, e.EhrDate, ert.Name, d.Firstname, d.Lastname, r.ReportID, r.ReportDate, r.ReportName, r.Author
										from Ehr as e 
										left join EHRReportType as ert on e.EhrReportID = ert.ID 
										left join Cases as c on e.CaseID = c.CaseID 
										left join Doctors as d on d.DoctorID = c.DoctorID 
										left join Reports as r on r.CaseID = c.CaseID 
										where e.CaseID='.$result->CaseID.'
										GROUP BY e.EhrID');
            if(sizeof($result->ehri) <= 1) {
                $result->ehri = NULL;
            }
        }
        return $results;
    }


}