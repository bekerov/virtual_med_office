<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 09.11.14
 * Time: 2:41
 */

class BillingTemplateController extends Controller {
    protected function getBillingTemplates() {
        $results = BillingTemplate::all();
        foreach ($results as $result) {
            $result->codes = BillingTemplateDesc::where('TemplateID', '=', $result->TemplateID)->get();
        }
        return $results;
    }

    protected function getBillingTemplate($id) {
		$results = BillingTemplate::find($id);
		
		$dCodes = array('A','B','C','D','E','F','G','H','I','J','K','L');
		$DiagnosisCodes = explode(',', $results->DiagnosisCodes);
		foreach($DiagnosisCodes as $k => $DiagnosisCode) {
			$Codes[@$dCodes[$k]] = $DiagnosisCode;
		}
		$results->code = $Codes;
		
		$results->items = BillingTemplateDesc::where('TemplateID', '=', $id)->get();
		foreach ($results->items as $item) {
			$Modifiers = explode(',', $item->Modifiers);
			foreach($Modifiers as $km => $Modifier) {
				$Modifiers[$km] = array( 'code' => $Modifier);
			}
			$item->Modifiers = $Modifiers;
			
			$digPointers = explode(',', $item->digPointers);
			foreach($digPointers as $kd => $digPointer) {
				$digPointers[$kd] = array( 'code' => $digPointer);
			}
			$item->digPointers = $digPointers;
			
			$item->Total = $item->Amount * $item->Units;
		}
        return $results;
    }

	protected function addBillingTemplate() {
        try {
			$bTemplate = new BillingTemplate;
            $bTemplate->Name = Input::get('Name');
			$dCodes = array('A','B','C','D','E','F','G','H','I','J','K','L');
			foreach($dCodes as $k => $code) {
				if(Input::get('code.'.$code)) {
					if($bTemplate->DiagnosisCodes != '') {
						$bTemplate->DiagnosisCodes .= ',';
					}
					$bTemplate->DiagnosisCodes .= Input::get('code.'.$code);
				}
			}
            $bTemplate->save();
			
			
			$item = 0;
			while($item >= 0) {
				if(Input::get('items.'.$item.'.CPTCode')) {
					$btDesc = new BillingTemplateDesc;
					$btDesc->TemplateID = $bTemplate->TemplateID;
					$btDesc->CPTCode = Input::get('items.'.$item.'.CPTCode');
					$btDesc->Amount = Input::get('items.'.$item.'.Amount');
					$btDesc->Units = Input::get('items.'.$item.'.Units');
					
					$iModifiers = 0;
					while($iModifiers >= 0) {
						if(Input::get('items.'.$item.'.Modifiers.'.$iModifiers.'.code')) {
							if($btDesc->Modifiers != '') {
								$btDesc->Modifiers .= ',';
							}
							$btDesc->Modifiers .= Input::get('items.'.$item.'.Modifiers.'.$iModifiers.'.code');
						} else {
							break;
						}
						++$iModifiers;
					}
					$idigPointers = 0;
					while($idigPointers >= 0) {
						if(Input::get('items.'.$item.'.digPointers.'.$idigPointers.'.code')) {
							if($btDesc->digPointers != '') {
								$btDesc->digPointers .= ',';
							}
							$btDesc->digPointers .= Input::get('items.'.$item.'.digPointers.'.$idigPointers.'.code');
						} else {
							break;
						}
						++$idigPointers;
					}
					$btDesc->save();
				} else {
					break;
				}
				++$item;
			}
			
        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

    protected function editBillingTemplate($id) {
        try {
			$bTemplate = BillingTemplate::find($id);
            $bTemplate->Name = Input::get('Name');
			$dCodes = array('A','B','C','D','E','F','G','H','I','J','K','L');
			foreach($dCodes as $k => $code) {
				if(Input::get('code.'.$code)) {
					if($bTemplate->DiagnosisCodes != '') {
						$bTemplate->DiagnosisCodes .= ',';
					}
					$bTemplate->DiagnosisCodes .= Input::get('code.'.$code);
				}
			}
            $bTemplate->save();
			
			BillingTemplateDesc::where('TemplateID', '=', $id)->delete();
			
			$item = 0;
			while($item >= 0) {
				if(Input::get('items.'.$item.'.CPTCode')) {
					$btDesc = new BillingTemplateDesc;
					$btDesc->TemplateID = $bTemplate->TemplateID;
					$btDesc->CPTCode = Input::get('items.'.$item.'.CPTCode');
					$btDesc->Amount = Input::get('items.'.$item.'.Amount');
					$btDesc->Units = Input::get('items.'.$item.'.Units');
					
					$iModifiers = 0;
					while($iModifiers >= 0) {
						if(Input::get('items.'.$item.'.Modifiers.'.$iModifiers.'.code')) {
							if($btDesc->Modifiers != '') {
								$btDesc->Modifiers .= ',';
							}
							$btDesc->Modifiers .= Input::get('items.'.$item.'.Modifiers.'.$iModifiers.'.code');
						} else {
							break;
						}
						++$iModifiers;
					}
					$idigPointers = 0;
					while($idigPointers >= 0) {
						if(Input::get('items.'.$item.'.digPointers.'.$idigPointers.'.code')) {
							if($btDesc->digPointers != '') {
								$btDesc->digPointers .= ',';
							}
							$btDesc->digPointers .= Input::get('items.'.$item.'.digPointers.'.$idigPointers.'.code');
						} else {
							break;
						}
						++$idigPointers;
					}
					$btDesc->save();
				} else {
					break;
				}
				++$item;
			}
			
        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }
	
	protected function statusBillingTemplate($id, $IsDeleted) {
        try {
			$bTemplate = BillingTemplate::find($id);
			$bTemplate->IsDeleted = $IsDeleted;
			$bTemplate->save();
		} catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
	}
}
