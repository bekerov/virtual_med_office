<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 09.11.14
 * Time: 2:41
 */

class UsersController extends Controller {
    protected function getUsers() {
        $results = Users::all();
        return $results;
    }

    protected function getUser($id) {
        $results = Users::find($id);
        return $results;
    }

    protected function addUser() {
        try {
            $patient = new Users;
            $patient->Name = Input::get('Name');
            $patient->Username = Input::get('Username');
            $patient->Password = Input::get('Password');
            $patient->Roles = Input::get('Roles');
            $patient->EmailId = Input::get('EmailId');
            $patient->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

    protected function editUser($id) {
        try {
            $patient = Users::find($id);
            $patient->Name = Input::get('Name');
            $patient->Username = Input::get('Username');
            $patient->Password = Input::get('Password');
            $patient->Roles = Input::get('Roles');
            $patient->EmailId = Input::get('EmailId');
            $patient->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }
    protected function statusUser($id) {
        try {
            $patient = Users::find($id);
            $patient->IsDeleted = Input::get('status');
            $patient->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

}
