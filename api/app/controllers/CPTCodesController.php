<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 09.11.14
 * Time: 2:41
 */

class CPTCodesController extends Controller {
    protected function getCPTCode($id) {
        $results = CPTCodes::where('CPTCode', '=', $id)->get();
        return $results[0];
    }

	protected function addCPTCode() {
		try {
            // Add CPTCodes
			$cptcodes = new CPTCodes;
			$cptcodes->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
		return  '{"status" : "success", "data" : null } ';
	}
}
