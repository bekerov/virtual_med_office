<?php
/**
 * User: Pershanin Dmitry
 * Date: 20.11.14
 */

class EhrController extends Controller {
    protected function getEhrs() {

        $results = DB::select(' select c.CaseID, c.PatientID, c.DOA, p.FirstName, p.LastName, p.DOB, p.SocialSecurityNumber
								from Cases as c, Patients as p 
								where p.PatientID = c.PatientID');
        foreach ($results as $result) {
            $result->DOA = ($result->DOA != NULL) ? $result->DOA : '-';
            $result->DOB = ($result->DOB != NULL) ? $result->DOB : '-';
            $result->ehri = DB::select('select e.EhrID, e.EhrDate, ert.Name, cal.firstName, cal.lastName
										from EHR as e 
										left join EHRReportType as ert on e.EhrReportID = ert.ID 
										left join Cases as c on e.CaseID = c.CaseID 
										left join Calendars as cal on cal.CalendarID = e.CalendarID 
										where e.CaseID='.$result->CaseID.'
										GROUP BY e.EhrID');
            $result->ehrr = DB::select("SELECT * FROM `Reports` WHERE `CaseID`='" . $result->CaseID . "' ORDER BY `ReportDate` ASC");

            if(sizeof($result->ehri) < 1) {
                $result->ehri = NULL;
            }
            if(sizeof($result->ehrr) < 1) {
                $result->ehrr = NULL;
            }
        }
        return $results;
    }
}