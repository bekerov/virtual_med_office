<?php
/**
 * User: Pershanin Dmitry
 * Date: 20.11.14
 */

class EhrTemplateController extends Controller {
    protected function getEhrs() {

        $results = DB::select('SELECT * FROM Procedures WHERE ProcedureID>0');
        foreach ($results as $result) {
            if($result->Duration!=NULL && $result->Duration!='' && $result->Duration>0)
			{
				$dur		=	$result->Duration/60;
				$rem		=	$result->Duration%60;
				$hr			=	floor($dur);
				$result->Duration	=	str_pad($hr,2,0,2).":".str_pad($rem,2,0,2);
			}
        }
        return $results;
    }
	
	protected function getEhrTemplate($ProcedureID) {
		$results = DB::select('SELECT * FROM Procedures WHERE ProcedureID='.$ProcedureID);
		foreach ($results as $result) {
            if($result->Duration!=NULL && $result->Duration!='' && $result->Duration>0)
			{
				$dur		=	$result->Duration/60;
				$rem		=	$result->Duration%60;
				$hr			=	floor($dur);
				$result->Duration	=	str_pad($hr,2,0,2).":".str_pad($rem,2,0,2);
				$result->procedures = $this->getEhrs();
				
				$result->EhrProcedures = DB::select("SELECT * FROM `EhrObjectives` WHERE `ID` in (".$result->EhrProcedures.") AND `ParentID`=0");
			}
        }
		return $results;
	}
	
	protected function EditEhrTemplate() {
		if ($_SERVER['REQUEST_METHOD'] == 'POST') 
					{
						$ProcedureId = $_POST["procedure"];
						$durNew=explode(":",$_POST["duration"]);
						$hrtoMin=$durNew[0]*60;
						$Duration = $hrtoMin+$durNew[1];
						if($ProcedureId!='')
						{
							$arrIds=array();
							foreach($_POST["cpt"] as $key=>$val)
							{
								if($val!='')
								{
									$insEHRObj=mysql_query("INSERT INTO `EhrObjectives` (`Name`,`CPTCode`) VALUES('".$_POST["name"][$key]."','".$val."')");
									$arrIds[]=mysql_insert_id();
								}
							}
							$ehrTests=implode(",",$arrIds);
							mysql_query("UPDATE `Procedures` SET `Duration`='" . $Duration . "',`EhrProcedures`='".$ehrTests."' WHERE `ProcedureID`='" . $ProcedureId . "'");
							$msg = 'Successfully Updated The EHR Template.';
						}
						/*if ($temId == 0) 
						{
							mysql_query("INSERT INTO `ehrnotestemplate` (`name`,`contant`,`type`) VALUES ('" . $Name . "','" . $Contant . "','" . $Type . "')");
							$additionalId = mysql_insert_id();
							$msg = 'Successfully Added a EHR Template.';
						} 
						else 
						{
							mysql_query("UPDATE `ehrnotestemplate` SET `Name`='" . $Name . "',`contant`='" . $Contant . "',`type`='" . $Type . "' WHERE `ID`='" . $temId . "'");
							//$queryDelDesc = mysql_query("DELETE FROM `AdditionalInfo` WHERE `ID`='" . $additionalId . "'");
							$msg = 'Successfully Updated The EHR Template.';
						}*/
					}
	}
	
}