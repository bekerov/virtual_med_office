<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 09.11.14
 * Time: 2:41
 */

class ProviderController extends Controller {
    protected function getProviders() {
        $results = ProviderInformation::all();
        return $results;
    }

    protected function getProvider($id) {
        $results = ProviderInformation::find($id);
        return $results;
    }

    protected function editProvider($id) {
        try {
            $patient = ProviderInformation::find($id);
            $patient->Name = Input::get('Name');
            $patient->Specialty = Input::get('Specialty');
            $patient->NPI = Input::get('NPI');
            $patient->License = Input::get('License');
            $patient->TaxID = Input::get('TaxID');
            $patient->Address = Input::get('Address');
            $patient->City = Input::get('City');
            $patient->State = Input::get('State');
            $patient->Zip = Input::get('Zip');
            $patient->Phone = Input::get('Phone');
            $patient->Fax = Input::get('Fax');
            $patient->Email = Input::get('Email');
            $patient->BName = Input::get('BName');
            $patient->BAddress = Input::get('BAddress');
            $patient->BCity = Input::get('BCity');
            $patient->BState = Input::get('BState');
            $patient->BZip = Input::get('BZip');
            $patient->BPhone = Input::get('BPhone');
            $patient->BFax = Input::get('BFax');
            $patient->BEmail = Input::get('BEmail');
            $patient->sameasPatient = Input::get('sameasPatient');
            $patient->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }
}
