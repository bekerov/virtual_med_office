<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 09.11.14
 * Time: 2:41
 */

class PatientsController extends Controller {
    protected function getPatients() {
        $results = Patients::all();
        return $results;
    }

    protected function getPatient($id) {
        $results = Patients::find($id);
        return $results;
    }

    protected function addPatient() {
        try {
            // Add patient
            $patient = new Patients;
            $patient->FirstName = Input::get('FirstName');
            $patient->MiddleName = Input::get('MiddleName');
            $patient->LastName = Input::get('LastName');
            $patient->Address = Input::get('Address');
            $patient->Address2 = Input::get('Address2');
            $patient->DOB = Input::get('DOB');
            $patient->SocialSecurityNumber = Input::get('SocialSecurityNumber');
            $patient->City = Input::get('City');
            $patient->Zip = Input::get('Zip');
            $patient->Phone = Input::get('Phone');
            $patient->Phone2 = Input::get('Phone2');
            $patient->Phone3 = Input::get('Phone3');
            $patient->EmergencyContactName = Input::get('EmergencyContactName');
            $patient->Notes = Input::get('Notes');
            $patient->Gender = Input::get('Gender');
            $patient->Driverslicense = Input::get('Driverslicense');
            $patient->State = Input::get('State');
            $patient->EmergencyPhone = Input::get('EmergencyPhone');
            $patient->save();

            // Add patient note
            $notes = new Notes;
            $notes->PatientID = $patient->PatientID;;
            $notes->NotesType = 'Patient';
            $notes->UserID = '0'; //todo Заменить когда будет готова авторизация
            $notes->Action = 'Patient Created';
            $notes->NoteDate = date("Y-m-d H:i:s");
            $notes->Notes = Input::get('Notes');
            $notes->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

    protected function editPatient($id) {
        try {
            $patient = Patients::find($id);
            $patient->FirstName = Input::get('FirstName');
            $patient->MiddleName = Input::get('MiddleName');
            $patient->LastName = Input::get('LastName');
            $patient->Address = Input::get('Address');
            $patient->Address2 = Input::get('Address2');
            $patient->DOB = Input::get('DOB');
            $patient->SocialSecurityNumber = Input::get('SocialSecurityNumber');
            $patient->City = Input::get('City');
            $patient->Zip = Input::get('Zip');
            $patient->Phone = Input::get('Phone');
            $patient->Phone2 = Input::get('Phone2');
            $patient->Phone3 = Input::get('Phone3');
            $patient->EmergencyContactName = Input::get('EmergencyContactName');
            $patient->Notes = Input::get('Notes');
            $patient->Gender = Input::get('Gender');
            $patient->Driverslicense = Input::get('Driverslicense');
            $patient->State = Input::get('State');
            $patient->EmergencyPhone = Input::get('EmergencyPhone');
            $patient->save();

            // Add patient note
            $notes = new Notes;
            $notes->PatientID = $patient->PatientID;;
            $notes->NotesType = 'Patient';
            $notes->UserID = '0'; //todo Заменить на ID пользователя, когда будет готова авторизация
            $notes->Action = 'Patient Edited';
            $notes->NoteDate = date("Y-m-d H:i:s");
            $notes->Notes = Input::get('Notes');
            $notes->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }
    protected function deletePatient($id) {
//        try {
//            $patient = Attorneys::find($id);
//            $patient->IsDeleted = Input::get('IsDeleted');
//            $patient->save();
//
//        } catch(PDOException $exception) {
//            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
//        }
//        return  '{"status" : "success", "data" : null } ';
    }

}
