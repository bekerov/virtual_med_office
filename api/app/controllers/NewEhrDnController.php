<?php
/**
 * User: Pershanin Dmitry
 * Date: 20.11.14
 */

class NewEhrDnController extends Controller {
    protected function getEhr($CaseID) {
        $results = DB::select(' select c.CaseID, c.PatientID, c.DOA, p.FirstName, p.LastName 
								from Cases as c, Patients as p
								where c.CaseID=' . $CaseID . ' GROUP BY c.CaseID');
        $results[0]->sDate = date('d/m/Y');
        $results[0]->procedures = DB::select('SELECT `ProcedureID`, `Name` FROM `Procedures` ORDER BY `ProcedureID` ASC');

        $results[0]->TempNotes = DB::select("SELECT `name`,`contant` FROM `ehrnotestemplate` WHERE `type`='notes' AND `IsDeleted`='n' ORDER BY `ID` ASC");
        $results[0]->TempSubjectives = DB::select("SELECT `name`,`contant` FROM `ehrnotestemplate` WHERE `type`='subjective' AND `IsDeleted`='n' ORDER BY `ID` ASC");
        $results[0]->TempAssesments = DB::select("SELECT `name`,`contant` FROM `ehrnotestemplate` WHERE `type`='assesment' AND `IsDeleted`='n' ORDER BY `ID` ASC");
        $results[0]->TempPlans = DB::select("SELECT `name`,`contant` FROM `ehrnotestemplate` WHERE `type`='plan' AND `IsDeleted`='n' ORDER BY `ID` ASC");

        $results[0]->calenders = DB::select("SELECT * FROM `Calendars` WHERE `IsDeleted`='n' ORDER BY `CalendarID` ASC");

        $lastTexts = DB::select('SELECT `Subjective`,`Assesment`,`Plan`,`Notes` FROM `EHR` ORDER BY `EhrID` DESC LIMIT 0,1');

        if(sizeof($lastTexts) > 0) {
            $results[0]->lastSubjective = $lastTexts[0]->Subjective;
            $results[0]->lastAssesment = $lastTexts[0]->Assesment;
            $results[0]->lastPlan = $lastTexts[0]->Plan;
            $results[0]->lastNotes = $lastTexts[0]->Notes;
        }

        return $results;
    }

    protected function addEhr() {
        try {
            // Add ehr
            $ehr = new Ehr;
            $ehr->PatientID = Input::get('0.PatientID');
            $ehr->CaseID = Input::get('0.CaseID');
            $ehr->Subjective = Input::get('0.Subjective');
            $ehr->Assesment = Input::get('0.Assesment');
            $ehr->Plan = Input::get('0.Plan');
            $ehr->Notes = Input::get('0.Notes');
            $ehr->CalendarID = Input::get('0.CalendarID');
            $ehr->ProcedureID = Input::get('0.ProcedureID');
            $ehr->EhrReportID = 2;
            $data = explode('/',Input::get('0.sDate'));
            $ehr->EhrDate = $data[2].'-'.$data[1].'-'.$data[0];
            $ehr->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }
}