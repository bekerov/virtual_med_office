<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 09.11.14
 * Time: 2:41
 */

class BillingController extends Controller {
    
	protected function  getBillings() {
        $results = DB::select('select c.CaseID, c.PatientID, c.DOA, p.FirstName, p.LastName, p.DOB, p.Phone, p.SocialSecurityNumber from Cases as c, Patients as p, BillBatches as bb join Bills as b on b.BatchID=bb.BatchID where c.PatientID=p.PatientID and c.CaseID=bb.CaseID GROUP BY bb.CaseID');
        foreach ($results as $result) {
            $result->billbatches = BillBatches::where('CaseID', '=', $result->CaseID)->with('statuses')->get();
            foreach ($result->billbatches as $bills) {
                $bills->bills = Bills::where('BatchID', '=', $bills->BatchID)->get(array('DOS', 'Units', 'Amount', 'AmtReceived'));
            }
        }
        return $results;
    }

	protected function getBilling($BatchID) {
		$results = DB::select('select c.CaseID, c.PatientID, c.DOA, p.FirstName, p.LastName, p.DOB, p.Phone, p.SocialSecurityNumber from Cases as c, Patients as p, BillBatches as bb join Bills as b on b.BatchID=bb.BatchID where c.PatientID=p.PatientID and c.CaseID=bb.CaseID AND bb.BatchID="'.$BatchID.'" GROUP BY bb.CaseID');
        foreach ($results as $result) {
            $result->billbatches = BillBatches::where('BatchID', '=', $BatchID)->with('statuses')->get();
            foreach ($result->billbatches as $bills) {
                $bills->bills = Bills::where('BatchID', '=', $bills->BatchID)->get(array('DOS', 'Units', 'Amount', 'AmtReceived'));
            }
        }
        return $results;
	}
	
	protected function addBill() {
		try {
            // Add BillBatch
			$billBatch = new BillBatches;
			$billBatch->BatchDate = date('Y-m-d', time());
			$billBatch->CaseID = Input::get('case_id');
			$billBatch->CalendarID = Input::get('calendar_id');
			$dCodes = array('A','B','C','D','E','F','G','H','I','J','K','L');
			foreach($dCodes as $k => $code) {
				if(Input::get('template.code.'.$code)) {
					if($billBatch->DiagnosisCodes != '') {
						$billBatch->DiagnosisCodes .= ',';
					}
					$billBatch->DiagnosisCodes .= Input::get('template.code.'.$code);
				}
			}
			$billBatch->Note = Input::get('Notes');
			$billBatch->PatientCondition = serialize(
				array(
					"Employment" => Input::get('Employment'), 
					"AutoAccident" => Input::get('AutoAccident'), 
					"OtherAccident" => Input::get('OtherAccident')
				)
			);
			$billBatch->PatientConditionState = Input::get('PatientConditionState');
			$billBatch->save();
			
			// Add Bill
			$item = 0;
			while($item >= 0) {
				if(Input::get('template.items.'.$item.'.CPTCode')) {
					$bills = new Bills;
					$bills->BatchID = $billBatch->BatchID;
					$bills->DOS = date('Y-m-d', strtotime(Input::get('template.items.'.$item.'.DOS')));
					$bills->CPTCode = Input::get('template.items.'.$item.'.CPTCode');
					$bills->Amount = Input::get('template.items.'.$item.'.Amount');
					$bills->Units = Input::get('template.items.'.$item.'.Units');
					$bills->Description = Input::get('template.items.'.$item.'.Description');
					
					$iModifiers = 0;
					while($iModifiers >= 0) {
						if(Input::get('template.items.'.$item.'.Modifiers.'.$iModifiers.'.code')) {
							if($bills->Modifiers != '') {
								$bills->Modifiers .= ',';
							}
							$bills->Modifiers .= Input::get('template.items.'.$item.'.Modifiers.'.$iModifiers.'.code');
						} else {
							break;
						}
						++$iModifiers;
					}
					$idigPointers = 0;
					while($idigPointers >= 0) {
						if(Input::get('template.items.'.$item.'.digPointers.'.$idigPointers.'.code')) {
							if($bills->digPointers != '') {
								$bills->digPointers .= ',';
							}
							$bills->digPointers .= Input::get('template.items.'.$item.'.digPointers.'.$idigPointers.'.code');
						} else {
							break;
						}
						++$idigPointers;
					}
					$dCodes = array('A','B','C','D','E','F','G','H','I','J','K','L');
					foreach($dCodes as $k => $code) {
						if(Input::get('template.code.'.$code)) {
							if($bills->digCodes != '') {
								$bills->digCodes .= ',';
							}
							$bills->digCodes .= Input::get('template.code.'.$code);
						}
					}
					$bills->save();
				} else {
					break;
				}
				++$item;
			}
            

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
		return  '{"status" : "success", "data" : null } ';
	}
}
