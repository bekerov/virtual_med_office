<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 09.11.14
 * Time: 15:00
 */

class CasesController extends Controller {
    protected function getAllCases() {
        $results = Cases::all();
        return $results;
    }

    protected function getCases($id) {
        $results = Cases::where('PatientID', '=', $id)->with(array('doctor', 'attorney', 'bills'))->get();
        foreach ($results as $result) {
            $result->case_insurances = CaseInsurances::where('CaseID', '=', $result->CaseID)->with('insurances')->get();
        }
        return $results;
    }

    protected function getCase($id) {
        $results = Cases::where('CaseID', '=', $id)->with(array('doctor', 'attorney', 'insurances'))->get()->first();
        $results->deleted = array();
        foreach ($results->insurances as $insurance) {
            $insurance->info = Insurances::find($insurance->InsuranceID);
        }
        return $results;
    }

    protected function editCase($id) {
        try {
            $item = Cases::find($id);
            $item->AttorneyID = Input::get('AttorneyID');
            $item->DOA = Input::get('DOA');
            $item->DoctorID = Input::get('DoctorID');
            $item->CaseType = Input::get('CaseType');
            $item->Notes = Input::get('Notes');
            $item->DiagnosisCodes = Input::get('DiagnosisCodes');
            $item->PatientWas = Input::get('PatientWas');
            $item->CaseStatus = Input::get('CaseStatus');
            $item->PolicyLimit = Input::get('PolicyLimit');
            $item->PolicyExhausted = Input::get('PolicyExhausted');
            $item->Deductible = Input::get('Deductible');
            $item->CoPay = Input::get('CoPay');
            $item->save();

            //Удаление страховок
            $deleted = Input::get('deleted');
            foreach ($deleted as  $key => $value) {
                $item = CaseInsurances::find($deleted[$key]['ID']);
                $item->delete();
            }

            $insurances = Input::get('insurances');

            foreach ($insurances as  $key => $value) {
                if ($insurances[$key]['ID'] === 0 ) {
                    // Создание страховки к кейсу
                    $item = new CaseInsurances;
                    $item->CaseID = $id;
                    $item->insType = $insurances[$key]['insType'];
                    $item->InsuranceID = $insurances[$key]['InsuranceID'];

                    //todo отрефакторить код, заменить на функцию проверки
                    if (isset($insurances[$key]['claim'])) { $item->claim = $insurances[$key]['claim']; }
                    if (isset($insurances[$key]['policy'])) { $item->policy = $insurances[$key]['policy']; }
                    if (isset($insurances[$key]['groupID'])) { $item->groupID = $insurances[$key]['groupID']; }
                    if (isset($insurances[$key]['PolicyHolder'])) { $item->PolicyHolder = $insurances[$key]['PolicyHolder']; }
                    if (isset($insurances[$key]['PH_FirstName'])) { $item->PH_FirstName = $insurances[$key]['PH_FirstName']; }
                    if (isset($insurances[$key]['PH_MiddleName'])) { $item->PH_MiddleName = $insurances[$key]['PH_MiddleName']; }
                    if (isset($insurances[$key]['PH_LastName'])) { $item->PH_LastName = $insurances[$key]['PH_LastName']; }
                    if (isset($insurances[$key]['PH_Address'])) { $item->PH_Address = $insurances[$key]['PH_Address']; }
                    if (isset($insurances[$key]['PH_Address2'])) { $item->PH_Address2 = $insurances[$key]['PH_Address2']; }
                    if (isset($insurances[$key]['PH_City'])) { $item->PH_City = $insurances[$key]['PH_City']; }
                    if (isset($insurances[$key]['PH_State'])) { $item->PH_State = $insurances[$key]['PH_State']; }
                    if (isset($insurances[$key]['PH_Zip'])) { $item->PH_Zip = $insurances[$key]['PH_Zip']; }
                    if (isset($insurances[$key]['PH_Phone'])) { $item->PH_Phone = $insurances[$key]['PH_Phone']; }
                    if (isset($insurances[$key]['PH_Relation'])) { $item->PH_Relation = $insurances[$key]['PH_Relation']; }
                    if (isset($insurances[$key]['adjusterName'])) { $item->adjusterName = $insurances[$key]['adjusterName']; }
                    if (isset($insurances[$key]['adjusterPhone'])) { $item->adjusterPhone = $insurances[$key]['adjusterPhone']; }
                    if (isset($insurances[$key]['adjusterFax'])) { $item->adjusterFax = $insurances[$key]['adjusterFax']; }
                    if (isset($insurances[$key]['adjPhoneExt'])) { $item->adjPhoneExt = $insurances[$key]['adjPhoneExt']; }
                    $item->save();
                } else {
                    $item =  CaseInsurances::find($insurances[$key]['ID']);
                    $item->insType = $insurances[$key]['insType'];
                    $item->InsuranceID = $insurances[$key]['InsuranceID'];
                    if (isset($insurances[$key]['claim'])) { $item->claim = $insurances[$key]['claim']; }
                    if (isset($insurances[$key]['policy'])) { $item->policy = $insurances[$key]['policy']; }
                    if (isset($insurances[$key]['groupID'])) { $item->groupID = $insurances[$key]['groupID']; }
                    if (isset($insurances[$key]['PolicyHolder'])) { $item->PolicyHolder = $insurances[$key]['PolicyHolder']; }
                    if (isset($insurances[$key]['PH_FirstName'])) { $item->PH_FirstName = $insurances[$key]['PH_FirstName']; }
                    if (isset($insurances[$key]['PH_MiddleName'])) { $item->PH_MiddleName = $insurances[$key]['PH_MiddleName']; }
                    if (isset($insurances[$key]['PH_LastName'])) { $item->PH_LastName = $insurances[$key]['PH_LastName']; }
                    if (isset($insurances[$key]['PH_Address'])) { $item->PH_Address = $insurances[$key]['PH_Address']; }
                    if (isset($insurances[$key]['PH_Address2'])) { $item->PH_Address2 = $insurances[$key]['PH_Address2']; }
                    if (isset($insurances[$key]['PH_City'])) { $item->PH_City = $insurances[$key]['PH_City']; }
                    if (isset($insurances[$key]['PH_State'])) { $item->PH_State = $insurances[$key]['PH_State']; }
                    if (isset($insurances[$key]['PH_Zip'])) { $item->PH_Zip = $insurances[$key]['PH_Zip']; }
                    if (isset($insurances[$key]['PH_Phone'])) { $item->PH_Phone = $insurances[$key]['PH_Phone']; }
                    if (isset($insurances[$key]['PH_Relation'])) { $item->PH_Relation = $insurances[$key]['PH_Relation']; }
                    if (isset($insurances[$key]['adjusterName'])) { $item->adjusterName = $insurances[$key]['adjusterName']; }
                    if (isset($insurances[$key]['adjusterPhone'])) { $item->adjusterPhone = $insurances[$key]['adjusterPhone']; }
                    if (isset($insurances[$key]['adjusterFax'])) { $item->adjusterFax = $insurances[$key]['adjusterFax']; }
                    if (isset($insurances[$key]['adjPhoneExt'])) { $item->adjPhoneExt = $insurances[$key]['adjPhoneExt']; }
                    $item->save();
                }

            }


        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

    protected function addCase($id) {
        try {
            // Add case to patient id
            $patient = new Cases;
            $patient->PatientID = $id;
            $patient->CaseType = Input::get('CaseType');
            $patient->AttorneyID = Input::get('AttorneyID');
            $patient->empName = Input::get('empName');
            $patient->empCity = Input::get('empCity');
            $patient->empZip = Input::get('empZip');
            $patient->PatientWas = Input::get('PatientWas');
            $patient->CaseStatus = Input::get('CaseStatus');
            $patient->PolicyExhausted = Input::get('PolicyExhausted');
            $patient->DOA = Input::get('DOA');
            $patient->empAdd = Input::get('empAdd');
            $patient->empPhone = Input::get('empPhone');
            $patient->diagnosisCodes = Input::get('diagnosisCodes');
            $patient->PolicyLimit = Input::get('PolicyLimit');
            $patient->Deductible = Input::get('Deductible');
            $patient->CoPay = Input::get('CoPay');
            $patient->save();

            // Add patient note
            $notes = new Notes;
            $notes->PatientID = $patient->PatientID;;
            $notes->NotesType = 'Case';
            $notes->UserID = '0'; //todo Заменить когда будет готова авторизация
            $notes->Action = 'Case Added';
            $notes->NoteDate = date("Y-m-d H:i:s");
            $notes->Notes = Input::get('Notes');
            $notes->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : {"CaseID":"'.$patient->CaseID.'"} } ';
    }


    protected function getInsurances() {
        $results = Insurances::all();
        return $results;
    }
    protected function getInsurance($id) {
        $results = Insurances::find($id);
        return $results;
    }

    protected function addInsurance() {
        try {
            // Add insurance
            $item = new Insurances;
            $item->Name = Input::get('Name');
            $item->Address = Input::get('Address');
            $item->Address2 = Input::get('Address2');
            $item->City = Input::get('City');
            $item->State = Input::get('State');
            $item->Zip = Input::get('Zip');
            $item->IsHealth = Input::get('IsHealth');
            $item->billingPhone = Input::get('billingPhone');
            $item->billingFax = Input::get('billingFax');
            $item->precertPhone = Input::get('precertPhone');
            $item->precertFax = Input::get('precertFax');
            $item->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

    // Добавление страховки к кейсу
    protected function addCaseInsurance($id) {
        try {
            // Add case insurance
            $item = new CaseInsurances;
            $item->CaseID = $id;
            $item->insType = Input::get('insType');
            $item->InsuranceID = Input::get('InsuranceID');
            $item->claim = Input::get('claim');
            $item->policy = Input::get('policy');
            $item->groupID = Input::get('groupID');
            $item->PolicyHolder = Input::get('PolicyHolder');
            $item->PH_FirstName = Input::get('PH_FirstName');
            $item->PH_MiddleName = Input::get('PH_MiddleName');
            $item->PH_LastName = Input::get('PH_LastName');
            $item->PH_Address = Input::get('PH_Address');
            $item->PH_Address2 = Input::get('PH_Address2');
            $item->PH_City = Input::get('PH_City');
            $item->PH_State = Input::get('PH_State');
            $item->PH_Zip = Input::get('PH_Zip');
            $item->PH_Phone = Input::get('PH_Phone');
            $item->PH_Relation = Input::get('PH_Relation');
            $item->adjusterName = Input::get('adjusterName');
            $item->adjusterPhone = Input::get('adjusterPhone');
            $item->adjusterFax = Input::get('adjusterFax');
            $item->adjPhoneExt = Input::get('adjPhoneExt');
            $item->save();

            //todo Добавить notes

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

    // Редактирование страховки к кейсу
    protected function editCaseInsurance($id) {
        try {
            // Edit case insurance
            $item =  CaseInsurances::find($id);
            $item->insType = Input::get('insType');
            $item->InsuranceID = Input::get('InsuranceID');
            $item->claim = Input::get('claim');
            $item->policy = Input::get('policy');
            $item->groupID = Input::get('groupID');
            $item->PolicyHolder = Input::get('PolicyHolder');
            $item->PH_FirstName = Input::get('PH_FirstName');
            $item->PH_MiddleName = Input::get('PH_MiddleName');
            $item->PH_LastName = Input::get('PH_LastName');
            $item->PH_Address = Input::get('PH_Address');
            $item->PH_Address2 = Input::get('PH_Address2');
            $item->PH_City = Input::get('PH_City');
            $item->PH_State = Input::get('PH_State');
            $item->PH_Zip = Input::get('PH_Zip');
            $item->PH_Phone = Input::get('PH_Phone');
            $item->PH_Relation = Input::get('PH_Relation');
            $item->adjusterName = Input::get('adjusterName');
            $item->adjusterPhone = Input::get('adjusterPhone');
            $item->adjusterFax = Input::get('adjusterFax');
            $item->adjPhoneExt = Input::get('adjPhoneExt');
            $item->save();

            //todo Добавить notes

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

}
