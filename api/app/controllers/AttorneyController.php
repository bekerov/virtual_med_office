<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 09.11.14
 * Time: 2:41
 */

class AttorneyController extends Controller {
    protected function getAttorneys() {
        $results = Attorneys::all();
        return $results;
    }

    protected function getAttorney($id) {
        $results = Attorneys::find($id);
        return $results;
    }

    protected function addAttorney() {
        try {
            $patient = new Attorneys;
            $patient->Practice = Input::get('Practice');
            $patient->Firstname = Input::get('Firstname');
            $patient->Lastname = Input::get('Lastname');
            $patient->Address = Input::get('Address');
            $patient->Address2 = Input::get('Address2');
            $patient->City = Input::get('City');
            $patient->State = Input::get('State');
            $patient->Zip = Input::get('Zip');
            $patient->Phone = Input::get('Phone');
            $patient->Phone2 = Input::get('Phone2');
            $patient->Fax = Input::get('Fax');
            $patient->Email = Input::get('Email');
            $patient->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

    protected function editAttorney($id) {
        try {
            $patient = Attorneys::find($id);
            $patient->Practice = Input::get('Practice');
            $patient->Firstname = Input::get('Firstname');
            $patient->Lastname = Input::get('Lastname');
            $patient->Address = Input::get('Address');
            $patient->Address2 = Input::get('Address2');
            $patient->City = Input::get('City');
            $patient->State = Input::get('State');
            $patient->Zip = Input::get('Zip');
            $patient->Phone = Input::get('Phone');
            $patient->Phone2 = Input::get('Phone2');
            $patient->Fax = Input::get('Fax');
            $patient->Email = Input::get('Email');
            $patient->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }
    protected function statusAttorney($id) {
        try {
            $patient = Attorneys::find($id);
            $patient->IsDeleted = Input::get('status');
            $patient->save();

        } catch(PDOException $exception) {
            return  '{"status" : "error", "message" : " Database error: '.$exception.'" } ';
        }
        return  '{"status" : "success", "data" : null } ';
    }

}
