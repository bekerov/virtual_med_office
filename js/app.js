/**
 * Created by Bekerov Artur on 04.11.14.
 */
'use strict';

/* App Module */

var crmApp = angular.module('crmApp', [
    'ngRoute',
    'crmControllers',
    'crmServices',
    'colorpicker.module'
]);

crmApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/schedule', {
                templateUrl: 'templates/schedule.html',
                controller: 'ScheduleCtrl'
            }).
            when('/patients', {
                templateUrl: 'templates/patients-list.html',
                controller: 'PatientsListCtrl'
            }).
            when('/billing', {
                templateUrl: 'templates/billing.html',
                controller: 'billingCtrl'
            }).
            when('/add_billing', {
                templateUrl: 'templates/add-billing.html',
                controller: 'addBillingCtrl'
            }).
            when('/add_billing/:CaseID', {
                templateUrl: 'templates/add-billing.html',
                controller: 'addBillingCtrl'
            }).
			when('/edit_billing/:BatchID', {
                templateUrl: 'templates/add-billing.html',
                controller: 'editBillingCtrl'
            }).
            when('/patient/:PatientId', {
                templateUrl: 'templates/patient-detail.html',
                controller: 'PatientDetailCtrl'
            }).
            when('/edit_patient/:PatientId', {
                templateUrl: 'templates/edit-patient.html',
                controller: 'editPatientCtrl'
            }).
            when('/add_patient', {
                templateUrl: 'templates/add-patient.html',
                controller: 'addPatientCtrl'
            }).
            when('/add_case/:PatientId', {
                templateUrl: 'templates/case.html',
                controller: 'addCaseCtrl'
            }).
            when('/edit_case/:CaseId', {
                templateUrl: 'templates/case.html',
                controller: 'editCaseCtrl'
            }).
            when('/attorneys', {
                templateUrl: 'templates/attorneys.html',
                controller: 'attorneysListCtrl'
            }).
            when('/attorney/:id', {
                templateUrl: 'templates/attorney.html',
                controller: 'attorneyDetailCtrl'
            }).
            when('/settings', {
                templateUrl: 'templates/settings.html',
                controller: 'settingsCtrl'
            }).
            when('/provider', {
                templateUrl: 'templates/provider.html',
                controller: 'providerCtrl'
            }).
            when('/provider/:id', {
                templateUrl: 'templates/provider-detail.html',
                controller: 'providerDetailCtrl'
            }).
            when('/collection', {
                templateUrl: 'templates/collection.html',
                controller: 'collectionCtrl'
            }).
			 when('/payment', {
                templateUrl: 'templates/payment.html',
                controller: 'paymentCtrl'
            }).
            when('/practicing_doctors', {
                templateUrl: 'templates/practicing-doctors.html',
                controller: 'practicingDoctorsCtrl'
            }).
            when('/practicing_doctors/:id', {
                templateUrl: 'templates/practicing-doctor-detail.html',
                controller: 'practicingDoctorDetailCtrl'
            }).
            when('/doctors', {
                templateUrl: 'templates/doctors-list.html',
                controller: 'doctorsListCtrl'
            }).
            when('/doctors/:id', {
                templateUrl: 'templates/doctor-detail.html',
                controller: 'doctorDetailCtrl'
            }).
            when('/case_types', {
                templateUrl: 'templates/case-types.html',
                controller: 'casetypesListCtrl'
            }).
            when('/case_types/:id', {
                templateUrl: 'templates/case-types-detail.html',
                controller: 'casetypesDetailCtrl'
            }).
            when('/billing_template', {
                templateUrl: 'templates/billing-template.html',
                controller: 'billingTemplateListCtrl'
            }).
            when('/billing_template/:id', {
                templateUrl: 'templates/billing-template-detail.html',
                controller: 'billingTemplateDetailCtrl'
            }).
            when('/users', {
                templateUrl: 'templates/users.html',
                controller: 'usersListCtrl'
            }).
            when('/users/:id', {
                templateUrl: 'templates/users-detail.html',
                controller: 'usersDetailCtrl'
            }).
            when('/ehr-all', {
                templateUrl: 'templates/ehr-list.html',
                controller: 'ehrListCtrl'
            }).
            when('/new-ehr-dn/:CaseID', {
                templateUrl: 'templates/new-ehr-dn.html',
                controller: 'newEhrDnCtrl'
            }).
            when('/new-medical-report/:id', {
                templateUrl: 'templates/new-medical-report.html',
                controller: 'newMedicalReportCtrl'
            }).
			when('/ehr_template', {
                templateUrl: 'templates/ehr-template.html',
                controller: 'ehrTemplateListCtrl'
            }).
			when('/ehr_template_edit/:ProcedureID', {
                templateUrl: 'templates/ehr-template-edit.html',
                controller: 'ehrTemplateEditCtrl'
            }).
            otherwise({
                redirectTo: '/patients'
            });
    }]);

crmApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

crmApp.filter('status', function () {
        return function (input) {
            if (input == 'y') {
                return 'Deleted'
            } else if (input == 'n') {
                return 'Active'
            }
        };
    });

crmApp.filter('doa', function() {
    return function(data) {
        return data === null ? 'No D.O.A.' : data;
    };
});

crmApp.filter('case_date', function() {
    return function(data) {
        return data === null ? '-' : data;
    };
});

crmApp.filter('insurance_type', function() {
    return function(data) {
        if (data === 'primary'){
            return 'P';
        } else if (data === 'secondary') {
            return 'S';
        } else {
            return 'T';
        }

    };
});
crmApp.filter('getById', function() {
    return function(input, id) {
        var i=0, len=input.length;
        for (; i<len; i++) {
            if (+input[i].id == +id) {
                return input[i];
            }
        }
        return null;
    }
});
crmApp.filter('capitalize', function() {
    return function(input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
});
