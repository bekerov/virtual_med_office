/**
 * Created by Bekerov Artur on 04.11.14.
 */
var crmServices = angular.module('crmServices', ['ngResource']);

crmServices.factory('Patients', ['$resource',
    function ($resource) {
        return $resource('api/public/patients/:PatientId', {}, {
            query: {method: 'GET', isArray: true,  cache: true},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);


crmServices.factory('Cases', ['$resource',
    function ($resource) {
        return $resource('api/public/cases/:PatientId', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);

crmServices.factory('Case', ['$resource',
    function ($resource) {
        return $resource('api/public/case/:CaseId', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);

crmServices.factory('Attorneys', ['$resource',
    function ($resource) {
        return $resource('api/public/attorneys/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);

crmServices.factory('Provider', ['$resource',
    function ($resource) {
        return $resource('api/public/provider/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);

crmServices.factory('Calendars', ['$resource',
    function ($resource) {
        return $resource('api/public/calendars/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('Doctors', ['$resource',
    function ($resource) {
        return $resource('api/public/doctors/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);

crmServices.factory('CaseTypes', ['$resource',
    function ($resource) {
        return $resource('api/public/casetypes/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('BillingTemplate', ['$resource',
    function ($resource) {
        return $resource('api/public/billing_template/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'POST'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('CPTCodes', ['$resource',
    function ($resource) {
        return $resource('api/public/cptcodes/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('Users', ['$resource',
    function ($resource) {
        return $resource('api/public/users/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('Billing', ['$resource',
    function ($resource) {
        return $resource('api/public/billing/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('Billbatches', ['$resource',
    function ($resource) {
        return $resource('api/public/billbatches/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);

crmServices.factory('Billbatches', ['$resource',
    function ($resource) {
        return $resource('api/public/billbatches/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('Payment', ['$resource',
    function ($resource) {
        return $resource('api/public/payments/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('Insurances', ['$resource',
    function ($resource) {
        return $resource('api/public/insurances/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: true},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('CaseInsurances', ['$resource',
    function ($resource) {
        return $resource('api/public/caseinsurances/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('Ehr', ['$resource',
    function ($resource) {
        return $resource('api/public/ehr/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('EhrTemplate', ['$resource',
    function ($resource) {
        return $resource('api/public/ehr_template/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: true},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('NewEhrDn', ['$resource',
    function ($resource) {
        return $resource('api/public/newehrdn/:CaseID', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('CalEvents', ['$resource',
    function ($resource) {
        return $resource('api/public/calevents/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);
crmServices.factory('AppointmentStatuses', ['$resource',
    function ($resource) {
        return $resource('api/public/appointment_statuses/:id', {}, {
            query: {method: 'GET', isArray: true,  cache: false},
            get: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            update: {method: 'PUT'},
            delete: {method: 'DELETE'}
        });
    }]);