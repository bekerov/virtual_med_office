/**
 * Created by Bekerov Artur on 04.11.14.
 */
'use strict';

/* Controllers */

//var crmControllers = angular.module('crmControllers', ['ui.bootstrap', 'ui.mask', 'mgcrea.ngStrap.typeahead']);
var crmControllers = angular.module('crmControllers', ['ui.bootstrap', 'ui.mask', 'ui.calendar']);

crmControllers.controller('PatientsListCtrl', ['$scope', '$http', 'Patients', 'Cases',
    function ($scope, $http, Patients, Cases) {
        // Get all patients
        $scope.patients = Patients.query();

        $scope.itemPerPage = 20;
        $scope.currentPage = 1;

        $scope.getCases = function(i) {
            $scope.patients[i].cases = Cases.query({PatientId: $scope.patients[i].PatientID });
        }

        $scope.getTotalBilled = function(data) {
            var total = 0
            for (var i=0; i < data.length; i++) {
                total += parseFloat(data[i].Amount) *parseFloat(data[i].Units);
            }
            return total;
        }
        $scope.getTotalPaid = function(data) {
            var total = 0
            for (var i=0; i < data.length; i++) {
                total += parseFloat(data[i].AmtReceived);
            }
            return total;
        }


    }]);

// Detail patient controller
crmControllers.controller('PatientDetailCtrl', ['$scope', '$http', 'Patients', '$routeParams', '$location',
    function ($scope, $http, Patients, $routeParams, $location) {

        $scope.patient = Patients.get({PatientId: $routeParams.PatientId});
    }]);

crmControllers.controller('addPatientCtrl', ['$scope', '$http', 'Patients', '$location',
    function ($scope, $http, Patients, $location) {
        // Format date
        $scope.today = function() {
            $scope.dt = new Date();
        };
//        $scope.today();
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.format = 'MM/dd/yyyy';
        // /Format date


        $scope.addPatient = function() {
            Patients.save({}, $scope.patient,
                function success(data) {
                    console.log('Patient created');
                    console.log(data);
                    $location.path("/patients/");
                },
                function err(error) {
                    console.log('Patient create error');
                });
        }
    }]);

crmControllers.controller('editPatientCtrl', ['$scope', '$http', 'Patients', '$routeParams',
    function ($scope, $http, Patients, $routeParams) {
        $scope.patient = Patients.get({PatientId: $routeParams.PatientId});

        // Format date
        $scope.today = function() {
            $scope.dt = new Date();
        };
//        $scope.today();
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.format = 'MM/dd/yyyy';
        // /Format date

        $scope.editPatient = function() {
            Patients.update({PatientId: $routeParams.PatientId}, $scope.patient,
                function success(data) {
                    console.log('Patient edited');
                    console.log(data);
                },
                function err(error) {
                    console.log('Patient edit error');
                });
        }
    }]);

//crmControllers.controller('addCaseCtrl', ['$scope', '$http', '$filter', 'Cases', 'Attorneys', '$routeParams', 'Doctors', 'Insurances',
//    function ($scope, $http, $filter, Cases, Attorneys, $routeParams, Doctors, Insurances) {
////        $scope.patient = Patients.get({PatientId: $routeParams.PatientId});
//
//        $scope.attorneys = Attorneys.query();
//
//        $scope.doctors = Doctors.query();
//
//        $scope.insurances = Insurances.query();
//
//        $scope.choosenInsurances = [{"id":1, "data":{}},{"id":2, "data":{}},{"id":3, "data":{}}];
//
//        $scope.isEmpty = function (obj) {
//            for (var i in obj) if (obj.hasOwnProperty(i)) return false;
//            return true;
//        };
//
//        // Format date
//        $scope.today = function() {
//            $scope.dt = new Date();
//        };
////        $scope.today();
//        $scope.clear = function () {
//            $scope.dt = null;
//        };
//        $scope.open = function($event) {
//            $event.preventDefault();
//            $event.stopPropagation();
//            $scope.opened = true;
//        };
//        $scope.dateOptions = {
//            formatYear: 'yy',
//            startingDay: 1
//        };
//        $scope.format = 'MM/dd/yyyy';
//        // /Format date
//
//        $scope.addCase = function() {
//            Cases.save({PatientId: $routeParams.PatientId}, $scope.case,
//                function success(data) {
//                    console.log('Case created');
//                    console.log(data);
//                },
//                function err(error) {
//                    console.log('Case save error');
//                });
//        }
//        $scope.addAttorneyForm = function() {
//            Attorneys.save({}, $scope.addAttorney,
//                function success(data) {
//                    console.log('Attorney created');
//                    console.log(data);
//                    $scope.attorneys = Attorneys.query();
//                },
//                function err(error) {
//                    console.log('Attorney save error');
//                });
//        }
//        $scope.addDoctorForm = function() {
//            Doctors.save({}, $scope.addDoctor,
//                function success(data) {
//                    console.log('Doctor created');
//                    console.log(data);
//                    $scope.doctors = Doctors.query();
//                },
//                function err(error) {
//                    console.log('Doctor save error');
//                });
//        }
//
//        $scope.addInsurance1 = function() {
//            $scope.choosenInsurances[0].data = $scope.insurance1;
//        }
//        $scope.addInsurance2 = function() {
//            $scope.choosenInsurances[1].data = $scope.insurance2;
//        }
//        $scope.addInsurance3 = function() {
//            $scope.choosenInsurances[2].data = $scope.insurance3;
//        }
//        $scope.newInsurance = function() {
//            Insurances.save({}, $scope.newinsurance,
//                function success(data) {
//                    console.log('Insurance created');
//                    console.log(data);
//                    $scope.insurances = Insurances.query();
//                },
//                function err(error) {
//                    console.log('Insurance save error');
//                });
//        }
//
//        $scope.$watch("insurance1.ID", function (value) {
//            $scope.insurance1.Name = $filter('getById')($scope.insurances, value);
//        });
//    }]);


crmControllers.controller('addCaseCtrl', function ($scope, $http, $filter, Case, Cases, Attorneys, $routeParams, Doctors, Insurances, CaseInsurances, $modal, $location) {

    $scope.case = {"insurances":[]}

    $scope.attorneys = Attorneys.query();
    $scope.doctors = Doctors.query();
    $scope.insurances = Insurances.query()

    $scope.selectedAttorney = function (model) {
        for (var i = 0; i < $scope.attorneys.length; i++) {
            if (model === $scope.attorneys[i].AttorneyID) {
                return $scope.attorneys[i].Lastname + ' ' + $scope.attorneys[i].Firstname;
            }
        }
    }


    // Format date
    $scope.today = function () {
        $scope.dt = new Date();
    };
    // scope.today();
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.format = 'MM/dd/yyyy';
    // /Format date

    $scope.editCase = function () {
        Cases.save({PatientId: $routeParams.PatientId}, $scope.case,
            function success(res) {
                console.log('Case saved');
//                console.log(data);
                $location.path("/edit_case/"+res.data.CaseID);

            },
            function err(error) {
                console.log('Case save error: ' + error);
            });
    }

    $scope.addAttorneyForm = function () {
        Attorneys.save({}, $scope.addAttorney,
            function success(data) {
                console.log('Attorney created');
                console.log(data);
                $scope.attorneys = Attorneys.query();
            },
            function err(error) {
                console.log('Attorney save error: ' + error);
            });
    }
    $scope.addDoctorForm = function () {
        Doctors.save({}, $scope.addDoctor,
            function success(data) {
                console.log('Doctor created');
                console.log(data);
                $scope.doctors = Doctors.query();
            },
            function err(error) {
                console.log('Doctor save error: ' + error);
            });
    }

    // Если true скрывает кнопку добавления страховки
    $scope.addButton = function(model) {
        for (var i=0; i< $scope.case.insurances.length; i++) {
            if (model === $scope.case.insurances[i].insType) {
                return true;
            }
        }
    }


    $scope.open1 = function (object) {
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: 'lg',
            resolve: {
                object: function () {
                    return object;
                }
            }
        });
//        modalInstance.result.then(function (editedObject) {
//            $scope.object = editedObject;
//        });
    };

    $scope.openInsuranceModal = function (object) {
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'addInsuranceModalInstanceCtrl',
            size: 'lg',
            resolve: {
                object: function () {
                    return object;
                }
            }
        });
        modalInstance.result.then(function (editedObject) {
            $scope.case.insurances.push(editedObject)
        });
    };
});


crmControllers.controller('editCaseCtrl', function ($scope, $http, $filter, Case, Cases, Attorneys, $routeParams, Doctors, Insurances, CaseInsurances, $modal) {

    $scope.attorneys = Attorneys.query();
    $scope.doctors = Doctors.query();
    $scope.insurances = Insurances.query();

    $scope.case = Case.get({CaseId: $routeParams.CaseId})

    $scope.selectedAttorney = function (model) {
        for (var i = 0; i < $scope.attorneys.length; i++) {
            if (model === $scope.attorneys[i].AttorneyID) {
                return $scope.attorneys[i].Lastname + ' ' + $scope.attorneys[i].Firstname;
            }
        }
    }


    $scope.removeInsurance = function(item) {
        var index = $scope.case.insurances.indexOf(item)
        $scope.case.insurances.splice(index, 1);

        $scope.case.deleted.push(item);

    }


    // Format date
    $scope.today = function () {
        $scope.dt = new Date();
    };
    // scope.today();
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.format = 'MM/dd/yyyy';
    // /Format date

    $scope.editCase = function () {
        Case.update({CaseId: $routeParams.CaseId}, $scope.case,
            function success(data) {
                console.log('Case updated');
                console.log(data);
            },
            function err(error) {
                console.log('Case update error: ' + error);
            });
    }

    $scope.addAttorneyForm = function () {
        Attorneys.save({}, $scope.addAttorney,
            function success(data) {
                console.log('Attorney created');
                console.log(data);
                $scope.attorneys = Attorneys.query();
            },
            function err(error) {
                console.log('Attorney save error: ' + error);
            });
    }
    $scope.addDoctorForm = function () {
        Doctors.save({}, $scope.addDoctor,
            function success(data) {
                console.log('Doctor created');
                console.log(data);
                $scope.doctors = Doctors.query();
            },
            function err(error) {
                console.log('Doctor save error: ' + error);
            });
    }

    // Если true скрывает кнопку добавления страховки
    $scope.addButton = function(model) {
        for (var i=0; i< $scope.case.insurances.length; i++) {
            if (model === $scope.case.insurances[i].insType) {
                return true;
            }
        }
    }


    $scope.open1 = function (object) {
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: 'lg',
            resolve: {
                object: function () {
                    return object;
                }
            }
        });
//        modalInstance.result.then(function (editedObject) {
//            $scope.object = editedObject;
//        });
    };

    $scope.openInsuranceModal = function (object) {
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'addInsuranceModalInstanceCtrl',
            size: 'lg',
            resolve: {
                object: function () {
                    return object;
                }
            }
        });
        modalInstance.result.then(function (editedObject) {
            $scope.case.insurances.push(editedObject)
        });
    };
});

// Контроллер редактирования страховки
crmControllers.controller('ModalInstanceCtrl', function ($scope, $modalInstance, object, Insurances) {

    $scope.object = object;
    $scope.insurances = Insurances.query();
    $scope.selectedInsurance = function(model) {
        for (var i=0; i< $scope.insurances.length; i++) {
            if (model === $scope.insurances[i].ID) {
                return $scope.insurances[i].Name;
            }
        }
    }
    $scope.ok = function () {
        $modalInstance.close($scope.object);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

// Контроллер добавления новой страховки
crmControllers.controller('addInsuranceModalInstanceCtrl', function ($scope, $modalInstance, object, Insurances) {

    $scope.object = {"ID":0, "insType": object, "PolicyHolder":"1", "PH_Relation": "Spouse"};

    $scope.insurances = Insurances.query();

    $scope.selectedInsurance = function(model) {
        for (var i=0; i< $scope.insurances.length; i++) {
            if (model === $scope.insurances[i].ID) {
                return $scope.insurances[i].Name;
            }
        }
    }


    $scope.ok = function () {
        $modalInstance.close($scope.object);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});





crmControllers.controller('attorneysListCtrl', ['$scope', '$http', 'Attorneys', '$routeParams',
    function ($scope, $http, Attorneys, $routeParams) {
        $scope.attorneys = Attorneys.query();
        $scope.itemPerPage = 20;
        $scope.currentPage = 1;

        $scope.deactivate = function(id) {
            Attorneys.delete({id: id.AttorneyID, status: 'y'});
            id.IsDeleted = 'y';
        }
        $scope.activate = function(id) {
            Attorneys.delete({id: id.AttorneyID, status: 'n'});
            id.IsDeleted = 'n';
        }
    }]);

crmControllers.controller('attorneyDetailCtrl', ['$scope', '$http', 'Attorneys', '$routeParams',
    function ($scope, $http, Attorneys, $routeParams) {
        if ($routeParams.id != 'new') {
            $scope.attorney = Attorneys.get({id: $routeParams.id});
        }

        $scope.editAttorney = function() {
            if ($routeParams.id == 'new') {
                Attorneys.save({}, $scope.attorney,
                    function success(data) {
                        console.log('Attorney created');
                        console.log(data);
                    },
                    function err(error) {
                        console.log('Attorney save error');
                    });
            } else {
                Attorneys.update({id: $routeParams.id}, $scope.attorney,
                    function success(data) {
                        console.log('Attorney updated');
                        console.log(data);
                    },
                    function err(error) {
                        console.log('Attorney update error');
                    });
            }
        }
    }]);


crmControllers.controller('settingsCtrl', ['$scope', '$http','$routeParams',
    function ($scope, $http,$routeParams) {

    }]);

crmControllers.controller('providerCtrl', ['$scope', '$http','$routeParams', 'Provider',
    function ($scope, $http,$routeParams, Provider) {
        $scope.providers = Provider.query()

    }]);

crmControllers.controller('providerDetailCtrl', ['$scope', '$http','$routeParams', 'Provider',
    function ($scope, $http,$routeParams, Provider) {
        $scope.provider = Provider.get({id: $routeParams.id})
        $scope.editProvider = function() {
            Provider.update({id: $routeParams.id}, $scope.provider,
                function success(data) {
                    console.log('Provider updated');
                    console.log(data);
                },
                function err(error) {
                    console.log('Provider update error');
                });
        }

        $scope.$watch("provider.Name", function (value) {
            if ($scope.provider.sameasPatient == 'yes') {
                $scope.provider.BName = value;
            }
        });
        $scope.$watch("provider.Address", function (value) {
            if ($scope.provider.sameasPatient == 'yes') {
                $scope.provider.BAddress = value;
            }
        });
        $scope.$watch("provider.State", function (value) {
            if ($scope.provider.sameasPatient == 'yes') {
                $scope.provider.BState = value;
            }
        });
        $scope.$watch("provider.City", function (value) {
            if ($scope.provider.sameasPatient == 'yes') {
                $scope.provider.BCity = value;
            }
        });
        $scope.$watch("provider.Zip", function (value) {
            if ($scope.provider.sameasPatient == 'yes') {
                $scope.provider.BZip = value;
            }
        });
        $scope.$watch("provider.Phone", function (value) {
            if ($scope.provider.sameasPatient == 'yes') {
                $scope.provider.BPhone = value;
            }
        });
        $scope.$watch("provider.Fax", function (value) {
            if ($scope.provider.sameasPatient == 'yes') {
                $scope.provider.BFax = value;
            }
        });
        $scope.$watch("provider.Email", function (value) {
            if ($scope.provider.sameasPatient == 'yes') {
                $scope.provider.BEmail = value;
            }
        });

    }]);


crmControllers.controller('collectionCtrl', ['$scope', '$http','$routeParams', 'Patients', 'Cases', 'Billbatches', 'Payment', '$location',
    function ($scope, $http,$routeParams, Patients, Cases, Billbatches, Payment, $location) {
        // Patient list
        $scope.patients = Patients.query();

        // Case list of this patient
        $scope.$watch("patient_id", function (value) {
            if (!isNaN(parseFloat(value)) && isFinite(value)) {
                $scope.cases = Cases.query({PatientId: value});
            }
        });
        // After select case
        $scope.$watch("case_id", function (value) {
            if (value) {
                $scope.collections = Billbatches.query({id: value})
            }
        });

        $scope.getTotalBilled = function(data) {
            var total = 0
            for (var i=0; i < data.length; i++) {
                total += parseFloat(data[i].Amount) *parseFloat(data[i].Units);
            }
            return total;
        }

        $scope.getBilledAmount = function(data) {
            return parseFloat(data.Units) * parseFloat(data.Amount);
        }

        $scope.payments = Payment.query({id:7})
        $scope.apply_payment = function() {
			$location.path("'/payment'");
        }
    }]);
	
crmControllers.controller('paymentCtrl', ['$scope', '$http','$routeParams', 'Patients', 'Cases', 'Billbatches', 'Payment',
    function ($scope, $http,$routeParams, Patients, Cases, Billbatches, Payment) {
		$scope.collect = {"payment":[], "patient":[]}
		$scope.collect.payment = Payment.query();
		
		$scope.collect.patient = Patients.get({PatientId: 1});
    }]);

crmControllers.controller('practicingDoctorsCtrl', ['$scope', '$http','$routeParams', 'Calendars',
    function ($scope, $http,$routeParams, Calendars) {
        $scope.doctors = Calendars.query();

        $scope.deactivate = function(id) {
            Calendars.delete({id: id.CalendarID, status: 'y'});
            id.IsDeleted = 'y';
        }
        $scope.activate = function(id) {
            Calendars.delete({id: id.CalendarID, status: 'n'});
            id.IsDeleted = 'n';
        }
    }]);

crmControllers.controller('practicingDoctorDetailCtrl', ['$scope', '$http','$routeParams', 'Calendars',
    function ($scope, $http,$routeParams, Calendars) {
        $scope.doctor = Calendars.get({id: $routeParams.id});

        $scope.editDoctor = function() {
            if ($routeParams.id == 'new') {
                Calendars.save({}, $scope.doctor,
                    function success(data) {
                        console.log('Calendar created');
                        console.log(data);
                    },
                    function err(error) {
                        console.log('Calendar save error');
                    });
            } else {
                Calendars.update({id: $routeParams.id}, $scope.doctor,
                    function success(data) {
                        console.log('Calendar updated');
                        console.log(data);
                    },
                    function err(error) {
                        console.log('Calendar update error');
                    });
            }
        }
    }]);


crmControllers.controller('doctorsListCtrl', ['$scope', '$http','$routeParams', 'Doctors',
    function ($scope, $http,$routeParams, Doctors) {
        $scope.doctors = Doctors.query();

        // Pagination
        $scope.itemPerPage = 20;
        $scope.currentPage = 1;
    }]);
crmControllers.controller('doctorDetailCtrl', ['$scope', '$http','$routeParams', 'Doctors',
    function ($scope, $http,$routeParams, Doctors) {
        $scope.doctor = Doctors.get({id: $routeParams.id});

        // Save if id = new, or update
        $scope.editDoctor = function() {
            if ($routeParams.id == 'new') {
                Doctors.save({}, $scope.doctor,
                    function success(data) {
                        console.log('Doctor created');
                        console.log(data);
                    },
                    function err(error) {
                        console.log('Doctor save error');
                    });
            } else {
                Doctors.update({id: $routeParams.id}, $scope.doctor,
                    function success(data) {
                        console.log('Doctor updated');
                        console.log(data);
                    },
                    function err(error) {
                        console.log('Doctor update error');
                    });
            }
        }
    }]);

crmControllers.controller('casetypesListCtrl', ['$scope', '$http','$routeParams', 'CaseTypes',
    function ($scope, $http,$routeParams, CaseTypes) {
        $scope.case_types = CaseTypes.query();
    }]);
crmControllers.controller('casetypesDetailCtrl', ['$scope', '$http','$routeParams', 'CaseTypes',
    function ($scope, $http,$routeParams, CaseTypes) {
        $scope.casetype = CaseTypes.get({id: $routeParams.id});

        // Save if id = new, or update
        $scope.editCaseType = function() {
            if ($routeParams.id == 'new') {
                CaseTypes.save({}, $scope.casetype,
                    function success(data) {
                        console.log('Case type created');
                        console.log(data);
                    },
                    function err(error) {
                        console.log('Case type save error');
                    });
            } else {
                CaseTypes.update({id: $routeParams.id}, $scope.casetype,
                    function success(data) {
                        console.log('Case type updated');
                        console.log(data);
                    },
                    function err(error) {
                        console.log('Case type update error');
                    });
            }
        }

    }]);

crmControllers.controller('billingTemplateListCtrl', ['$scope', '$http','$routeParams', 'BillingTemplate',
    function ($scope, $http,$routeParams, BillingTemplate) {
        $scope.templates = BillingTemplate.query();
		
		$scope.statusTemplate = function(template) {
			template.IsDeleted = (template.IsDeleted == 'y' ? 'n':'y');
			BillingTemplate.delete({id: template.TemplateID, IsDeleted: template.IsDeleted});
        }
    }]);
	
crmControllers.controller('billingTemplateDetailCtrl', ['$scope', '$http','$routeParams', 'BillingTemplate', '$parse', 'CPTCodes', '$location',
    function ($scope, $http,$routeParams, BillingTemplate, $parse, CPTCodes, $location) {
        
		if($routeParams.id != 'new') {
			$scope.template = BillingTemplate.get({id: $routeParams.id});
		} else {
			$scope.template = {"items":[]}
			$scope.template.items = [{"Modifiers":[{}], "digPointers":[{}]}]
		}
		
		$scope.addItem = function() {
            $scope.template.items.push({"Modifiers":[{}], "digPointers":[{}]});
        }
		$scope.delItem = function(itemIndex) {
            if($scope.template.items.length > 1) {
				$scope.template.items.splice(itemIndex, 1);
			} else {
				alert('Single record can not be deleted');
			}
        }
        $scope.addMod = function(data) {
            data.push({});
        }
		$scope.delMod = function(mod, itemModifiers) {
            if(itemModifiers.length > 1) {
				itemModifiers.splice(mod, 1);
			} else {
				alert('Single record can not be deleted');
			}
        }
        $scope.addPoint = function(data) {
            data.push({});
        }
		$scope.delPoint = function(point, itemdigPointers) {
            if(itemdigPointers.length > 1) {
				itemdigPointers.splice(point, 1);
			} else {
				alert('Single record can not be deleted');
			}
        }
		
		$scope.cptCodeChange = function(item) {
            CPTCodes.get({id: item.item.CPTCode},
                function success(data) {
                    item.item.Description = data.Description;
					item.item.Amount = data.Amount;
					console.log('CPTCode got');
                    console.log(data);
                },
                function err(error) {
                    console.log('CPTCode get error');
                });
        }
		
        // Save if id = new, or update
        $scope.editTemplate = function() {
			if ($routeParams.id == 'new') {
                BillingTemplate.save({}, $scope.template,
                    function success(data) {
                        console.log('Template created');
                        console.log(data);
						$location.path("'/billing_template/'");
                    },
                    function err(error) {
                        console.log('Template save error');
                    });
            } else {
                BillingTemplate.update({id: $routeParams.id}, $scope.template,
                    function success(data) {
                        console.log('Template updated');
                        console.log(data);
						$location.path("'/billing_template/'");
                    },
                    function err(error) {
                        console.log('Template update error');
                    });
            }
        }

    }]);

crmControllers.controller('usersListCtrl', ['$scope', '$http','$routeParams', 'Users',
    function ($scope, $http,$routeParams, Users) {
        $scope.users = Users.query();

        $scope.itemPerPage = 20;
        $scope.currentPage = 1;

        $scope.deactivate = function(id) {
            Users.delete({id: id.UserID, status: 'y'});
            id.IsDeleted = 'y';
        }
        $scope.activate = function(id) {
            Users.delete({id: id.UserID, status: 'n'});
            id.IsDeleted = 'n';
        }
    }]);
crmControllers.controller('usersDetailCtrl', ['$scope', '$http','$routeParams', 'Users',
    function ($scope, $http,$routeParams, Users) {
        $scope.user = Users.get({id: $routeParams.id});

        // Save if id = new, or update
        $scope.editUser = function() {

            if ($routeParams.id == 'new') {
                Users.save({}, $scope.user,
                    function success(data) {
                        console.log('User created');
                        console.log(data);
                    },
                    function err(error) {
                        console.log('User save error');
                    });
            } else {
                Users.update({id: $routeParams.id}, $scope.user,
                    function success(data) {
                        console.log('User updated');
                        console.log(data);
                    },
                    function err(error) {
                        console.log('User update error');
                    });
            }
        }

    }]);

crmControllers.controller('billingCtrl', ['$scope', '$http','$routeParams', 'Billing',
    function ($scope, $http,$routeParams, Billing) {
        $scope.billings = Billing.query();

        $scope.totalBilled = function(data){
            var total = 0
            for (var i=0; i < data.length; i++) {
                total += parseInt(data[i].Amount, 10);
            }
            return total;
        }
        $scope.totalPaid = function(data){
            var total = 0
            for (var i=0; i < data.length; i++) {
                total += parseInt(data[i].AmtReceived, 10);
            }
            return total;
        }

    }]);

crmControllers.controller('addBillingCtrl', ['$scope', '$http','$routeParams', 'Billing', 'Patients', 'Cases', 'Calendars', '$location', 'BillingTemplate', 'CPTCodes', 
    function ($scope, $http, $routeParams, Billing, Patients, Cases, Calendars, $location, BillingTemplate, CPTCodes) {
		$scope.billing = {"template":[]}
		$scope.billing.template = {"items":[]}
		$scope.billing.template.items = [{"Modifiers":[{}], "digPointers":[{}]}]
		
		$scope.addItem = function() {
            $scope.billing.template.items.push({"Modifiers":[{}], "digPointers":[{}]});
        }
		$scope.delItem = function(itemIndex) {
            if($scope.billing.template.items.length > 1) {
				$scope.billing.template.items.splice(itemIndex, 1);
			} else {
				alert('Single record can not be deleted');
			}
        }
        $scope.addMod = function(data) {
            data.push({});
        }
		$scope.delMod = function(mod, itemModifiers) {
            if(itemModifiers.length > 1) {
				itemModifiers.splice(mod, 1);
			} else {
				alert('Single record can not be deleted');
			}
        }
        $scope.addPoint = function(data) {
            data.push({});
        }
		$scope.delPoint = function(point, itemdigPointers) {
            if(itemdigPointers.length > 1) {
				itemdigPointers.splice(point, 1);
			} else {
				alert('Single record can not be deleted');
			}
        }
		
		$scope.cptCodeChange = function(item) {
            CPTCodes.get({id: item.item.CPTCode},
                function success(data) {
					item.item.Amount = data.Amount;
					item.item.Total = Number(item.item.Amount) * Number(item.item.Units);
					console.log('CPTCode got');
                    console.log(data);
                },
                function err(error) {
                    console.log('CPTCode get error');
                });
        }
		
		//Templates list
		$scope.billing.templates = BillingTemplate.query();
		
		$scope.loadTemplate = function(TemplateID) {
            $scope.billing.template = BillingTemplate.get({id: TemplateID});
        }
		
        if($routeParams.CaseID != 'new') {
			//Single case
			$scope.billing.case_id = $routeParams.CaseID;
		} else {
			// Patient list
			$scope.patients = Patients.query();

			// Case list of this patient
			$scope.$watch("billing.patient_id", function (value) {
				if (!isNaN(parseFloat(value)) && isFinite(value)) {
					$scope.cases = Cases.query({PatientId: value});
				}
			});
		}
		
		//Doctors list
		$scope.calendars = Calendars.query();
		

		$scope.addBill = function() {
            Billing.save({BillingId: $routeParams.BillingId}, $scope.billing,
                function success(data) {
                    console.log('Billing created');
                    console.log(data);
					$location.path("'/billing/'");
                },
                function err(error) {
                    console.log('Billing save error');
                });
        }

    }]);
	
crmControllers.controller('editBillingCtrl', ['$scope', '$http','$routeParams', 'Billing', 'Patients', 'Cases', 'Calendars', '$location', 'BillingTemplate', 'CPTCodes', 
    function ($scope, $http, $routeParams, Billing, Patients, Cases, Calendars, $location, BillingTemplate, CPTCodes) {
		$scope.billing = {"template":[]}
		$scope.billing.template = {"items":[]}
		$scope.billing.template.items = [{"Modifiers":[{}], "digPointers":[{}]}]
		
		$scope.addItem = function() {
            $scope.billing.template.items.push({"Modifiers":[{}], "digPointers":[{}]});
        }
		$scope.delItem = function(itemIndex) {
            if($scope.billing.template.items.length > 1) {
				$scope.billing.template.items.splice(itemIndex, 1);
			} else {
				alert('Single record can not be deleted');
			}
        }
        $scope.addMod = function(data) {
            data.push({});
        }
		$scope.delMod = function(mod, itemModifiers) {
            if(itemModifiers.length > 1) {
				itemModifiers.splice(mod, 1);
			} else {
				alert('Single record can not be deleted');
			}
        }
        $scope.addPoint = function(data) {
            data.push({});
        }
		$scope.delPoint = function(point, itemdigPointers) {
            if(itemdigPointers.length > 1) {
				itemdigPointers.splice(point, 1);
			} else {
				alert('Single record can not be deleted');
			}
        }
		
		$scope.cptCodeChange = function(item) {
            CPTCodes.get({id: item.item.CPTCode},
                function success(data) {
					item.item.Amount = data.Amount;
					item.item.Total = Number(item.item.Amount) * Number(item.item.Units);
					console.log('CPTCode got');
                    console.log(data);
                },
                function err(error) {
                    console.log('CPTCode get error');
                });
        }
		
		//Templates list
		$scope.billing.templates = BillingTemplate.query();
		
		$scope.loadTemplate = function(TemplateID) {
            $scope.billing.template = BillingTemplate.get({id: TemplateID});
        }
		
        
		// Patient list
		$scope.patients = Patients.query();

		// Case list of this patient
		$scope.$watch("billing.patient_id", function (value) {
			if (!isNaN(parseFloat(value)) && isFinite(value)) {
				$scope.cases = Cases.query({PatientId: value});
			}
		});
		
		//Doctors list
		$scope.calendars = Calendars.query();
		
		Billing.get({id: $routeParams.BatchID},
			function success(data) {
				console.log(data);
				console.log('Billing loaded');
			},
			function err(error) {
				console.log('Billing save error');
			});

		$scope.editBill = function() {
            Billing.save({BillingId: $routeParams.BillingId}, $scope.billing,
                function success(data) {
                    console.log('Billing edited');
                    console.log(data);
					$location.path("'/billing/'");
                },
                function err(error) {
                    console.log('Billing save error');
                });
        }

    }]);

crmControllers.controller('ehrListCtrl', ['$scope', '$http', '$routeParams', 'Ehr',
    function ($scope, $http, $routeParams, Ehr) {
        // Get all ehrs
        $scope.ehrs = Ehr.query();

        $scope.itemPerPage = 20;
        $scope.currentPage = 1;

    }]);

crmControllers.controller('ehrTemplateListCtrl', ['$scope', '$http', '$routeParams', 'EhrTemplate',
    function ($scope, $http, $routeParams, EhrTemplate) {
        // Get all ehrts
        $scope.ehrts = EhrTemplate.query();
    }]);
	
crmControllers.controller('ehrTemplateEditCtrl', ['$scope', '$http', '$routeParams', 'EhrTemplate',
    function ($scope, $http, $routeParams, EhrTemplate) {
        // Get all ehrts
        $scope.ehrts = EhrTemplate.get({id: $routeParams.ProcedureID});
    }]);

crmControllers.controller('newEhrDnCtrl', ['$scope', '$http', '$routeParams', 'NewEhrDn', '$location', 'EhrTemplate',
    function ($scope, $http, $routeParams, NewEhrDn, $location, EhrTemplate) {
        $scope.ehrdn = NewEhrDn.query({CaseID: $routeParams.CaseID});

		$scope.procedureChange = function(ehrdni) {
            EhrTemplate.get({id: ehrdni.ProcedureID},
                function success(data) {
                    var ii = 0 ;
					ehrdni.EhrProcedures = data[0].EhrProcedures;
                    console.log(ehrdni.EhrProcedures);
                },
                function err(error) {
                    console.log('CPTCode get error');
                });
        }
        
		$scope.addNewEhrDN = function() {
            NewEhrDn.save({}, $scope.ehrdn,
                function success(data) {
                    console.log('NewEhrDN created');
                    console.log(data);
                    $location.path("'/ehr-all/'");
                },
                function err(error) {
                    console.log('NewEhrDN create error');
                });
        }

    }]);

crmControllers.controller('newMedicalReportCtrl', ['$scope', '$http', '$routeParams', 'NewMedicalReport',
    function ($scope, $http, $routeParams, NewMedicalReport) {
        $scope.newMedicalReport = NewMedicalReport.get({id: $routeParams.id});
    }]);
