/**
 * Created by Bekerov Artur on 01.12.14.
 */
//crmControllers.controller('ScheduleCtrl', function ($scope, $http, $filter, $modal) {
//
//})

function ScheduleCtrl($scope,$compile,uiCalendarConfig, $modal, CalEvents) {

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $scope.calendar_events = CalEvents.query();

    /* Change View */
    $scope.changeView = function(view,calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
    };
    /* Change View */
    $scope.renderCalender = function(calendar) {
        if(uiCalendarConfig.calendars[calendar]){
            uiCalendarConfig.calendars[calendar].fullCalendar('render');
        }
    };
    /* Render Tooltip */
    $scope.eventRender = function( event, element, view ) {
        element.attr({'tooltip': event.title,
            'tooltip-append-to-body': true});
        $compile(element)($scope);
    };

    // Окно при нажатии на дату
    $scope.alertOnDateClick = function(date, jsEvent, view) {
        localStorage.setItem('chosendate', date);
        //localStorage.getItem('currentCompaniesPage')

        $scope.openChoosePatientModal();
    }


    $scope.events = [
        {title: 'All Day Event',start: '2014-09-12 10:30:00',allDay: false},
        {title: 'Long Event',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
        {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
        {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
        {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
        {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
    ];
    /* event source that calls a function on every view switch */
    $scope.eventsF = function (start, end, timezone, callback) {
        var s = new Date(start).getTime() / 1000;
        var e = new Date(end).getTime() / 1000;
        var m = new Date(start).getMonth();
        var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
        callback(events);
    };

//    $scope.calEventsExt = {
//        color: '#f00',
//        textColor: 'yellow',
//        events: [
//            {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
//            {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
//            {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
//        ]
//    };
    /* alert on eventClick */
    $scope.alertOnEventClick = function( date, jsEvent, view){
        $scope.alertMessage = (date.title + ' was clicked ');
    };
    /* alert on Drop */
    $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
        $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };
    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
        $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };
    /* add and removes an event source of choice */
//    $scope.addRemoveEventSource = function(sources,source) {
//        var canAdd = 0;
//        angular.forEach(sources,function(value, key){
//            if(sources[key] === source){
//                sources.splice(key,1);
//                canAdd = 1;
//            }
//        });
//        if(canAdd === 0){
//            sources.push(source);
//        }
//    };
    /* add custom event*/
//    $scope.addEvent = function() {
//        $scope.events.push({
//            title: 'Open Sesame',
//            start: new Date(y, m, 28),
//            end: new Date(y, m, 29),
//            className: ['openSesame']
//        });
//    };
    /* remove event */
//    $scope.remove = function(index) {
//        $scope.events.splice(index,1);
//    };


    /* config object */
    $scope.uiConfig = {
        calendar:{
            height: 450,
            editable: false,
            header:{
                left: 'today prev,next',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            dayClick: $scope.alertOnDateClick,
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender
        }
    };

    /* event sources array*/
    $scope.eventSources = [{events: $scope.calendar_events}];
//    $scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];


    // Открытие модального окна Create Block
    $scope.openCreateBlockModal = function (object) {
        var modalInstance = $modal.open({
            templateUrl: 'addBlockModal.html',
            controller: 'CreateBlockModalInstanceCtrl',
            size: 'lg'
        });
//        modalInstance.result.then(function (editedObject) {
//            console.log(editedObject);
//        });
    };

    // Открытие модального окна Choose patient
    $scope.openChoosePatientModal = function (object) {
        var modalInstance = $modal.open({
            templateUrl: 'choosePatient.html',
            controller: 'ChoosePatientModalInstanceCtrl',
            size: 'sm'
        });
        modalInstance.result.then(function (patient_type) {
            if (patient_type == 'new' ) {
                alert('Sorry, try later')
            } else if ( patient_type == 'existing' ) {

                // открывает модальное окно для существующего пациента
                $scope.openExistingPatientModal();
            }
        });
    };

    // Открытие модального окна для существующего пациента
    $scope.openExistingPatientModal = function () {
        var modalInstance = $modal.open({
            templateUrl: 'existingPatient.html',
            controller: 'ExistingPatientInstanceCtrl',
            size: 'lg'
        });
        modalInstance.result.then(function (editedObject) {

        });
    };

}


// Контроллер Create block
crmControllers.controller('CreateBlockModalInstanceCtrl', function ($scope, $modalInstance, Calendars, CalEvents) {

    $scope.calendars = Calendars.query();

    $scope.block = {};
    $scope.block.StartTime = new Date();

    var duration = new Date();
    duration.setHours(0);
    duration.setMinutes(30);
    $scope.block.duration = duration;

    // Datepicker
    $scope.today = function () {
        $scope.dt = new Date();
    };
    // scope.today();
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.format = 'MM/dd/yyyy';
    // /Datepicker


    $scope.$watch("block.date", function(newValue, oldValue){
        if (newValue) {
            day = newValue.getDate();
            month = newValue.getMonth();
            year = newValue.getFullYear();

            $scope.block.StartTime.setDate(day);
            $scope.block.StartTime.setMonth(month);
            $scope.block.StartTime.setFullYear(year);
        }
    });

    $scope.saveCalEvent = function() {
        CalEvents.save({}, $scope.block,
            function success(data) {
                console.log('Calendar event created');
                $modalInstance.close();
            },
            function err(error) {
                console.log('Calendar event save error');
            });
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

// Контроллер модального окна, выбора типа пациента
crmControllers.controller('ChoosePatientModalInstanceCtrl', function ($scope, $modalInstance) {
    // передает выбранный параметр в новое модальное окно
    $scope.new_patient = function () {
        $modalInstance.close('new');
    }
    $scope.existing_patient = function () {
        $modalInstance.close('existing');
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


// Контроллер модального окна добавления события для существующего пациента
crmControllers.controller('ExistingPatientInstanceCtrl', function ($scope, $modalInstance, Patients, Case, Cases, AppointmentStatuses, Calendars, $modal) {

    $scope.patients = Patients.query();
    $scope.calendars = Calendars.query();
    $scope.appointment_statuses = AppointmentStatuses.query();

    $scope.appointment = {};
    var duration = new Date();
    duration.setHours(0);
    duration.setMinutes(30);
    $scope.appointment.duration = duration;

    $scope.selectedPatient = function (model) {
        for (var i = 0; i < $scope.patients.length; i++) {
            if (model === $scope.patients[i].PatientID) {
                return $scope.patients[i].LastName + ' ' + $scope.patients[i].FirstName;
            }
        }
    }



    // Открытие модального окна для добавления страховки
    $scope.open1 = function (object) {
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: 'lg',
            resolve: {
                object: function () {
                    return object;
                }
            }
        });
//        modalInstance.result.then(function (editedObject) {
//            $scope.object = editedObject;
//        });
    };

    // Если true скрывает кнопку добавления страховки
    $scope.addButton = function(model) {
        for (var i=0; i< $scope.case.insurances.length; i++) {
            if (model === $scope.case.insurances[i].insType) {
                return true;
            }
        }
    }
    // Открывает модальное окно добавления страховки
    $scope.openInsuranceModal = function (object) {
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'addInsuranceModalInstanceCtrl',
            size: 'lg',
            resolve: {
                object: function () {
                    return object;
                }
            }
        });
        modalInstance.result.then(function (editedObject) {
            $scope.case.insurances.push(editedObject)
        });
    };

    // Datepicker
    $scope.today = function () {
        $scope.dt = new Date();
    };
    // scope.today();
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.format = 'MM/dd/yyyy';
    // /Datepicker


    $scope.$watch("patients.PatientID", function(newValue, oldValue){
        if (newValue == parseInt(newValue, 10)) {
            $scope.patient = Patients.get({PatientId: newValue});
            $scope.cases = Cases.query({PatientId: newValue});
        }
    });

    // Для списка insurance
    $scope.case = {"insurances":[]}

    // Если выбран insurance , то вставляет во вкладу claim
    $scope.$watch("patients.case_id", function(newValue, oldValue){
        if (newValue == parseInt(newValue, 10)) {
            $scope.case = Case.get({CaseId: newValue})
        } else {
            $scope.case = {"insurances":[]}
        }
    });




    $scope.ok = function () {
        $modalInstance.close($scope.object);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


// Контроллер добавления/редактирования страховки
crmControllers.controller('ModalInstanceCtrl', function ($scope, $modalInstance, object, Insurances) {

    $scope.object = object;
    $scope.insurances = Insurances.query();
    $scope.selectedInsurance = function(model) {
        for (var i=0; i< $scope.insurances.length; i++) {
            if (model === $scope.insurances[i].ID) {
                return $scope.insurances[i].Name;
            }
        }
    }
    $scope.ok = function () {
        $modalInstance.close($scope.object);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});